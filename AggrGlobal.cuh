/*
 * AggrGlobal.cuh
 *
 *  Created on: 13/01/2015
 *      Author: gonzalez
 */

#ifndef AGGRGLOBAL_CUH_
#define AGGRGLOBAL_CUH_

#define WITHOUT_DOUBLE_ATOMIC 1
#define WITHOUT_SHUFFLE 0
#define WITHOUT_DOUBLE_SHUFFLE 0

#define WARP_SIZE 32
#define VIRT_WARP_SIZE 8

#if VIRT_WARP_SIZE == 1
#define MASK 0x00000001
#elif VIRT_WARP_SIZE == 2
#define MASK 0x00000003
#elif VIRT_WARP_SIZE == 4
#define MASK 0x0000000f
#elif VIRT_WARP_SIZE == 8
#define MASK 0x000000ff
#elif VIRT_WARP_SIZE == 16
#define MASK 0x0000ffff
#else
#define MASK 0xffffffff
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////   Aggregated and Global Memory: Only one leader ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ inline int lane_id(void) { return threadIdx.x % WARP_SIZE; }
__device__ inline int virt_lane_id(void) { return threadIdx.x % VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id(void) { return threadIdx.x / VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id2(void) { return lane_id() / VIRT_WARP_SIZE; }

// Atomic add for doubles
#if WITHOUT_DOUBLE_ATOMIC
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                        __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

// Shuffle using doubles (Julien Demouth version)
#if WITHOUT_DOUBLE_SHUFFLE
__device__ __inline__ double __shfl(double x, int lane, int vw_size)
{
// Split the double number into 2 32b registers.
int lo, hi;
asm volatile( "mov.b64 {%0,%1}, %2;" : "=r"(lo), "=r"(hi) : "d"(x));
// Shuffle the two 32b registers.
 lo = __shfl(lo, lane, vw_size);
 hi = __shfl(hi, lane, vw_size);
// Recreate the 64b number.
asm volatile( "mov.b64 %0, {%1,%2};" : "=d"(x) : "r"(lo), "r"(hi));
return x;
}
#endif

// Warp broadcast
template <class T>
__device__ T warp_bcast(T v, int leader, int vw_size) {
#if WITHOUT_SHUFFLE
    __shared__ volatile T _v[BLOCK_SIZE/VIRT_WARP_SIZE]; // One element for each virtual warp

    if ( virt_lane_id() == leader )
    	_v[virt_warp_id()] = v;

    return _v[virt_warp_id()]; // No sync, unsafe
}
#else
	return __shfl(v, leader, vw_size);
}
#endif

// warp-aggregated atomic add (reduction, w/o return value)
template <class T>
__device__ inline void atomicAggAdd(T *ctr, T value){
#if WITHOUT_SHUFFLE
unsigned int vmask = __ballot(1) & ( MASK << (virt_warp_id2() * VIRT_WARP_SIZE)); // Consider active threads belonging to this virtual warp
// select the leader
int leader = __ffs(vmask) - 1; // leader will be 0 if all threads active
// leader does the update
T res = value;
int next = leader;

// Add value from every thread in the virtual warp
while(__popc(vmask)-1 > 0){
  // Update mask
  vmask ^= (1 << next);
  next = __ffs(vmask) - 1;

  int next2 = next % VIRT_WARP_SIZE;
  res += warp_bcast(value, next2, VIRT_WARP_SIZE);
}

if ( lane_id() == leader )
  atomicAdd(ctr, res);
}
#else
// leader is thread 0 of virtual warp
  T res = value;
  int next = 0;

  // Add value from every thread in the virtual warp
  while(next < VIRT_WARP_SIZE - 1){
    next++;
    res += warp_bcast(value, next, VIRT_WARP_SIZE);
  }

  if(virt_lane_id() == 0)
    atomicAdd(ctr, res);
}
#endif


// Sparse Matrix Vector Product en GM
// This routine assumes a COO format matrix

template <typename IndexType,
		  typename ValueType>
__global__ void coo_agg_atomic_spmv(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
	
	for (int iter=0; iter<GRANULARITY; iter++) {
	
	const IndexType idx = iter* gridDim.x * blockDim.x + blockIdx.x*blockDim.x+threadIdx.x;

	if ( idx < num_nonzeros )
	{
	
		const IndexType rowidx = ARow[idx];
		const IndexType rowidx_leader = warp_bcast(rowidx, 0, VIRT_WARP_SIZE);
		const IndexType colidx = ACol[idx];
		const ValueType value = Aval[idx]*b[colidx];
		
		if ( rowidx_leader == rowidx )
			atomicAggAdd(&y[rowidx], value);
		else
			atomicAdd(&y[rowidx], value);

	}
	}

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////// Irregular reduction in Global Memory: several leaders are sequentially processed ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename ValueType> 
__device__ void GM_aggregated_ireduction_float(ValueType *dst, const ValueType v, const int k) 
{
    int lane_id = threadIdx.x & 31;
      
	uint acc_mask;
	int leader = 0; 
	
	acc_mask = 0;

	while (leader >= 0){ // Process sequentially each leader

		const int k_leader = __shfl(k, leader, WARP_SIZE);
		
		const int dif = k - k_leader;
		uint mask = ~ __ballot(dif); // Threads with the same key valu3
	 
		ValueType v1;
		if (dif == 0)
			v1=v; // value is saved
		else
			v1=0;  // v1 is the thread value for the treads with the same key value, elsewhere 0
		
		const int lsb = __ffs(mask); // least significant bit position
		const int msb = WARP_SIZE - __clz(mask); // most significant bit position
		const int distance = msb-lsb; // distance between mosy distant bits
		
		ValueType vaux; // Shuffle output
		
		if (distance > 0) {
			vaux=__shfl_down(v1, 1, WARP_SIZE);
			if (lane_id < 31) // This operation avoid adding data beyond warp limits
				v1 = vaux + v1;
		
			if (distance > 1) {
				vaux=__shfl_down(v1, 2, WARP_SIZE);
				if (lane_id < 30)
					v1 = vaux + v1;

				if (distance > 3) {
					vaux=__shfl_down(v1, 4, WARP_SIZE);
					if (lane_id < 28)
						v1 = vaux + v1;

					if (distance > 7) {
						vaux=__shfl_down(v1, 8, WARP_SIZE);
						if (lane_id < 24)
							v1 = vaux + v1;
		
						if (distance > 15) {
							vaux=__shfl_down(v1, 16, WARP_SIZE);
							if (lane_id < 16)
								v1 = vaux + v1;
						}
					}
				}
			}
		}
	
		if (lane_id == leader)
			atomicAdd(&dst[k], v1);
		
		acc_mask |= mask; // Lanes already processed
	
		leader = __ffs(~acc_mask)-1; 
	}
}


//Aggregated with several leaders: new method and GM
 
template  <typename IndexType,
		  typename ValueType>
__global__ void coo_agg_ireduction_GM(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
	
	for (int iter=0; iter<GRANULARITY; iter++) {
	
	const IndexType idx = iter* gridDim.x * blockDim.x  + blockIdx.x*blockDim.x+threadIdx.x;

	if ( idx < num_nonzeros )
	{
		const IndexType rowidx = ARow[idx];
		const IndexType colidx = ACol[idx];
		const ValueType value = Aval[idx]*b[colidx];

		GM_aggregated_ireduction_float(y, value, rowidx);
		
		
	}
	}

}


/****************************************************************************************/
/** Method proposed by Elmar Wetsphal: Voting and Shuffling for fewer Atomic Operations ***/
/******************************************************************************************/

template<typename IndexType> 
__device__ __inline__ uint get_peers(const IndexType my_key) { 
  uint peers; 
  bool is_peer; 
  uint unclaimed=0xffffffff;                   // in the beginning, no threads are claimed
  do {
    const IndexType other_key=__shfl(my_key,__ffs(unclaimed)-1);
// get key from least unclaimed lane 
    is_peer=(my_key==other_key);               // do we have a match?
    peers=__ballot(is_peer);
                   // find all matches                 
    unclaimed^=peers;                          // matches are no longer unclaimed
  } while (!is_peer);                          // repeat as long as we haven’t found our match
  return peers; 
}

template <typename IndexType,
			typename ValueType> 
__device__ __inline__ ValueType add_peers(ValueType *dest, const IndexType my_key, ValueType x, uint peers)
 { 
  int lane= threadIdx.x & 31; 
  int first=__ffs(peers)-1;              // find the leader
  int rel_pos=__popc(peers<<(32-lane));  // find our own place
  peers&=(0xfffffffe<<lane);             // drop everything to our right
  while(__any(peers)) {                  // stay alive as long as anyone is working
    int next=__ffs(peers);               // find out what to add
    ValueType t=__shfl(x,next-1);                // get what to add (undefined if nothing)
    if (next)                            // important: only add if there really is anything
      x+=t;
    int done=rel_pos&1;                  // local data was used in iteration when its LSB is set
    peers&=__ballot(!done);              // clear out all peers that were just used
    rel_pos>>=1;                         // count iterations by shifting position
  } 
  if (lane==first)                       // only leader threads for each key perform atomics 
    atomicAdd(dest+my_key,x);                   
	ValueType res=__shfl(x,first);				  // distribute result (if needed) 
  return res;                            // may also return x or return value of atomic, as needed
} 


//Aggregated with several leaders
 
template  <typename IndexType,
		  typename ValueType>
__global__ void coo_Westphal_atomics_GM(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
	
	for (int iter=0; iter<GRANULARITY; iter++) {
	
		const IndexType idx = iter* gridDim.x * blockDim.x  + blockIdx.x*blockDim.x+threadIdx.x;
	
		if ( idx < num_nonzeros )
		{
			const IndexType rowidx = ARow[idx];
			const IndexType colidx = ACol[idx];
			const ValueType value = Aval[idx]*b[colidx];

			const uint peers = get_peers(rowidx);
		
			add_peers(y, rowidx, value, peers);
		
		
		}
	}

}

///////////////////////////////////////////////////////////////////////////////////
/////   Aggregated, Shared memory and Global Memory: Only one leader     /////////
/////   This implementation assumes keys are increasingly ordered   /////////
////	and there are small or no gaps between index values               ////////
//////////////////////////////////////////////////////////////////////////////////

template <typename IndexType,
		  typename ValueType>
__global__ void SMGM_coo_agg_atomic_spmv_ver2(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
	__shared__ ValueType smem[SM_KEYS+BLOCK_SIZE]; // SM_KEYS for voting, BLOCK_SIZE for backup
	__shared__ volatile int first_key, last_key;
  
	for (int i =0;i<(SM_KEYS + 2* BLOCK_SIZE -1)/BLOCK_SIZE; i++)
		if (i*BLOCK_SIZE + threadIdx.x < SM_KEYS + BLOCK_SIZE)
			smem[i*BLOCK_SIZE+threadIdx.x] = 0; // Reset local memory
  
	int idx_start = blockIdx.x * blockDim.x * GRANULARITY; // Now 
	int idx_end = (blockIdx.x+1) * blockDim.x * GRANULARITY;
	if (idx_end > num_nonzeros)
		idx_end = num_nonzeros + (BLOCK_SIZE - num_nonzeros % BLOCK_SIZE);

	if (threadIdx.x == 0){
		first_key = 100000000; // Here a number higher that the highest key
		last_key = -1; // Here a number lower that the lowest key
	}
  	__syncthreads();
  
	for (int iter=idx_start; iter<idx_end; iter+=blockDim.x) {
	
		const IndexType idx =  iter + threadIdx.x;
			
		if (idx < num_nonzeros) {
		
			const IndexType rowidx= ARow[idx];			
			const IndexType colidx = ACol[idx];
			const ValueType value = Aval[idx]*b[colidx];
			
			if (threadIdx.x == 0) 
				if (rowidx < first_key)
					first_key = rowidx; //The lowest key value currently voting in SM 
			
			if (threadIdx.x == blockDim.x - 1)
				if (rowidx > last_key)
					last_key = rowidx; // The highest key value currently voting in SM
			
			if (idx == num_nonzeros -1) // Last iteration of the last block
				if (rowidx > last_key)
					last_key = rowidx;
		
			const IndexType rowidx_leader = warp_bcast(rowidx, 0, VIRT_WARP_SIZE);
			if ( rowidx_leader == rowidx )
				atomicAggAdd(&smem[rowidx % (SM_KEYS+BLOCK_SIZE)], value);
			else 
				atomicAdd(&smem[rowidx % (SM_KEYS+BLOCK_SIZE)], value);	
				
			//if (rowidx == 22218) // matrix cop20K_A (many row with no data) 
			//	printf("Block=%d Thid=%d Val=%f\n", blockIdx.x, threadIdx.x, value);
				
		}
		
		__syncthreads();
		
		if (last_key - first_key +1 >= SM_KEYS){ // SM_KEYS buffer is run off space -> save data
			
			for (int i =0;i<((last_key-first_key+1) + BLOCK_SIZE -1)/BLOCK_SIZE; i++) {
			
				const int ind = i*BLOCK_SIZE + threadIdx.x;
				const int pos = first_key + ind;
				
			//	if (threadIdx.x == 0 && first_key <= 22218 && last_key >= 22218)
			//		printf("smem=%f  iter=%d fisrt_key=%d last_key=%d\n", smem[22218%(SM_KEYS+BLOCK_SIZE)], ((last_key-first_key+1) + BLOCK_SIZE -1)/BLOCK_SIZE, first_key, last_key);
				
				if (ind <= (last_key-first_key)){
				
					if (ind == last_key-first_key || ind == 0)
						atomicAdd(&y[pos], smem[ pos % (SM_KEYS+BLOCK_SIZE)]);
					else
						y[pos] = smem[ pos % (SM_KEYS+BLOCK_SIZE)];

					smem[ pos % (SM_KEYS+BLOCK_SIZE)] = 0; 
				}
			}
			
			__syncthreads();
			
			 if (threadIdx.x == 0){ // Establish new values for the new keys
				first_key = 100000000; 
				last_key = -1;
			}
			
			__syncthreads();
			
		}	
			
  }
  
	if (first_key != 100000000) { // Save the data in SM if they have not been saved yet
  
		for (int i =0;i<((last_key-first_key+1) + BLOCK_SIZE -1)/BLOCK_SIZE; i++) {
			if (i*BLOCK_SIZE + threadIdx.x <= (last_key-first_key)){				
			
				const int ind = i*BLOCK_SIZE + threadIdx.x;
				const int pos = first_key + ind;
			
				if (ind == last_key-first_key || ind == 0)
					atomicAdd(&y[pos], smem[pos % (SM_KEYS+BLOCK_SIZE)]);
				else
					y[pos] = smem[pos % (SM_KEYS+BLOCK_SIZE)];
			}
		}
	}
	
}

///////////////////////////////////////////////////////////////////////////////////
/////  Shared memory and Global Memory: Only one leader     /////////
/////   This implementation assumes keys are increasingly ordered   /////////
////	and there are small or no gaps between index values               ////////
//////////////////////////////////////////////////////////////////////////////////

template <typename IndexType,
		  typename ValueType>
__global__ void SMGM_coo_atomic_spmv(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
  __shared__ float smem[SM_KEYS+BLOCK_SIZE]; // BLOCK_SIZE for backup
  __shared__ int first_key, last_key;
  
  for (int i =0;i<(SM_KEYS + 2* BLOCK_SIZE -1)/BLOCK_SIZE; i++)
			if (i*BLOCK_SIZE + threadIdx.x < SM_KEYS + BLOCK_SIZE)
				smem[i*BLOCK_SIZE+threadIdx.x] = 0; // Reset local memory
  
  int idx_start = blockIdx.x * blockDim.x * GRANULARITY;
  int idx_end = (blockIdx.x+1) * blockDim.x * GRANULARITY;
  if (idx_end > num_nonzeros)
	idx_end = num_nonzeros + (BLOCK_SIZE - num_nonzeros % BLOCK_SIZE);

  if (threadIdx.x == 0){
	first_key = 100000000; // Here a number higher that the highest key
	last_key = -1;
  }
  	__syncthreads();
  
  for (int iter=idx_start; iter<idx_end; iter+=blockDim.x) {
	
		const IndexType idx =  iter + threadIdx.x;
			
		if (idx < num_nonzeros) {
		
			const IndexType rowidx= ARow[idx];			
			const IndexType colidx = ACol[idx];
			const ValueType value = Aval[idx]*b[colidx];
			
			if (threadIdx.x == 0) 
				if (rowidx < first_key)
					first_key = rowidx;
			
			if (threadIdx.x == blockDim.x - 1)
				if (rowidx > last_key)
					last_key = rowidx;
			
			if (idx == num_nonzeros -1)
				if (rowidx > last_key)
					last_key = rowidx;
			
			atomicAdd(&smem[rowidx % (SM_KEYS+BLOCK_SIZE)], value);	
				
		}
		
		__syncthreads();
		
		if (last_key - first_key +1 >= SM_KEYS){ // SM_KEYS buffer is run off space -> save data
			
			for (int i =0;i<((last_key-first_key+1) + BLOCK_SIZE -1)/BLOCK_SIZE; i++) {
				if (i*BLOCK_SIZE + threadIdx.x <= (last_key-first_key)){

				const int ind = i*BLOCK_SIZE + threadIdx.x;
				const int pos = first_key + ind;
				
				if (ind == last_key-first_key || ind == 0)
						atomicAdd(&y[pos], smem[ pos % (SM_KEYS+BLOCK_SIZE)]);
					else
						y[pos] = smem[ pos % (SM_KEYS+BLOCK_SIZE)];

				smem[(first_key+i*BLOCK_SIZE + threadIdx.x) % (SM_KEYS+BLOCK_SIZE)] = 0;
				}
			}
			
			__syncthreads();
			
			 if (threadIdx.x == 0){
				first_key = 100000000; // Here a number higher that the highest key
				last_key = -1;
			}
			
			__syncthreads();
			
		}	
			
  }
  
	if (first_key != 100000000) {
  
		for (int i =0;i<((last_key-first_key+1) + BLOCK_SIZE -1)/BLOCK_SIZE; i++) {
			if (i*BLOCK_SIZE + threadIdx.x <= (last_key-first_key)){

				const int ind = i*BLOCK_SIZE + threadIdx.x;
				const int pos = first_key + ind;
				
				if (ind == last_key-first_key || ind == 0)
						atomicAdd(&y[pos], smem[ pos % (SM_KEYS+BLOCK_SIZE)]);
					else
				y[pos] = smem[ pos % (SM_KEYS+BLOCK_SIZE)];

			}
		}
	}
	
}





// key must have a value between 0 and NUMBER_OF_KEYS -1

__device__ void SMGM_aggregated_ireduction_float(float *dst, float v, const int k) 
{
  
  __shared__ float smem[BLOCK_SIZE]; 
  __shared__ int first_key, last_key;
  
   if (threadIdx.x == 0) 
		first_key = k;
	
	smem[threadIdx.x] = 0;
		
	__syncthreads();
  
  int lane_id = threadIdx.x - (threadIdx.x / WARP_SIZE) * WARP_SIZE;
      
	uint mask, acc_mask;
	int k_leader, leader = 0; 
	
	acc_mask = 0;
	
		
	while (leader >= 0){
		
		k_leader = __shfl(k, leader, WARP_SIZE);
		
		int dif = k - k_leader;
		mask = ~ __ballot(dif); // Threads with the same key value
	
		int my_mask = (mask >> lane_id) & 0x1; // my_mask is 1 for the threads with the same key vale, else 0
 
		float v1 = v * (float)my_mask; // v1 is the thread value for the treads with the same key value, else 0
		
		float vaux;
/*
		int lsb = __ffs(mask); // least significant bit position
		int msb = WARP_SIZE - __clz(mask); // most significant bit position
		int distance = msb-lsb+1;
		int iterations = WARP_SIZE-(__clz(distance)+1); // iterations for the shuffle
	
		if ((1<<iterations) < distance)
			iterations++; // Correct the number of iterations 
			*/
		
		int iterations = 5;
		for (int j = 0; j < iterations ; j++) {
		
			vaux=__shfl_down(v1, 1 << j, WARP_SIZE);
			
			if (lane_id + (1<<j) >= WARP_SIZE ) // If shfl beyond warp limits
				vaux=0;
		 
			v1 = vaux + v1;
		
		//	 if (lane_id == leader)
		//		 printf ("wid=%d id=%d v=%f distance=%d iterations=%d\n", warp_id, lane_id, v1, distance, j);
		}
		
		if (lane_id == leader)
			//smem[k] += v1;
			atomicAdd(&smem[k-first_key], v1);
		
		acc_mask |= mask; // Lanes already processed
	
		leader = __ffs(~acc_mask)-1; 
		
	}
	
	
	 if (threadIdx.x == blockDim.x - 1)
		last_key = k;
	
	__syncthreads();
	
	int number_of_keys = last_key-first_key+1;
	
	//if (threadIdx.x == 0)
		if (number_of_keys > BLOCK_SIZE)
		number_of_keys=BLOCK_SIZE;
			
			//printf("warning %d %d\n", first_key, last_key);
	
	/*int num_iter = number_of_keys / BLOCK_SIZE;  
	if ( (number_of_keys % BLOCK_SIZE) != 0)
	  num_iter++;
  
	for (int i=0; i<num_iter; i++)
	  if (threadIdx.x + i * BLOCK_SIZE < number_of_keys)
		atomicAdd(&dst[threadIdx.x + i*BLOCK_SIZE], smem[threadIdx.x+i*BS]);*/
		
	if (threadIdx.x < number_of_keys)
		atomicAdd(&dst[threadIdx.x + first_key], smem[threadIdx.x]);
	
}







#endif /* AGGRGLOBAL_CUH_ */
