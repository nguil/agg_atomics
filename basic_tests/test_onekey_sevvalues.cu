
int free_values(DataType *h_value, DataType *d_value,  int *h_key, int *d_key, DataType *h_result, DataType *d_result, 
						DataType *correct_results)
{
	if (h_value != NULL) 
		free(h_value);
		
	if (d_value != NULL)
		cudaFree((void *)d_value);
		
	if (h_key != NULL)
		free((void *)h_key);
		
	if (d_key != NULL)
		cudaFree((void *)d_key);
		
	if (h_result != NULL)
		free((void *)h_result);
		
	if (d_result != NULL)
		cudaFree((void *)d_result);
		
	return 0;
}

int initialize_values(DataType **h_value, DataType **d_value,  int **h_key, int **d_key, DataType **h_result, DataType **d_result, 
						DataType **correct_results, int numElements)
{
	cudaError_t err = cudaSuccess;
	DataType *lh_value, *ld_value, *lh_result, *ld_result, *lcorrect_results;
	int *lh_key, *ld_key;
	
    // Allocate the host input vector A
    lh_value = (DataType *)malloc(numElements*sizeof(DataType));
	lh_key = (int *)malloc(numElements*sizeof(int));

    // Allocate the host output vectors
	lh_result = (DataType *)malloc(numElements*sizeof(DataType));

    // Verify that allocations succeeded
    if (lh_value == NULL || lh_key == NULL || lh_result == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vectors
    ld_value = NULL;
    err = cudaMalloc((void **)&ld_value, numElements*sizeof(DataType));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	ld_key = NULL;
    err = cudaMalloc((void **)&ld_key, numElements*sizeof(int));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
	
    // Allocate the device output vectors
	ld_result = NULL;
    err = cudaMalloc((void **)&ld_result, numElements*sizeof(DataType));
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
    }
    
	// Give values to the input vectors and calculates de correct result
	
	//printf ("Number of entry elements = %d\n", numElements);
	
	lcorrect_results = (DataType *)calloc(NUMBER_OF_KEYS, sizeof(DataType));
	for (int i=0; i<numElements; i++) {
		lh_value[i] = (DataType) 1; //(DataType)(i % (NUMBER_OF_KEYS+7));
		lh_key[i]= i/10  % NUMBER_OF_KEYS;
		lcorrect_results[lh_key[i]] += lh_value[i];
	}
	
	*h_value = lh_value;
	*d_value = ld_value;
	*h_key = lh_key;
	*d_key = ld_key;
	*h_result = lh_result;
	*d_result = ld_result;
	*correct_results = lcorrect_results;
	
	return 0;
	
}

int send_data_to_device(DataType *h_value, DataType *d_value,  int *h_key, int *d_key, DataType *d_result, int numElements)
{ 
	cudaError_t err = cudaSuccess;

	err = cudaMemcpy(d_value, h_value, numElements*sizeof(DataType), cudaMemcpyHostToDevice);
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	
	err = cudaMemcpy(d_key, h_key, numElements*sizeof(int), cudaMemcpyHostToDevice);
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	
	// Reset result
	err = cudaMemset(d_result, 0x0, NUMBER_OF_KEYS*sizeof(DataType));
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to initialize device counter nres (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	
	return 0;
}

int get_data_and_check(DataType *h_result, DataType *d_result, DataType *correct_results)
{
	cudaError_t err = cudaSuccess;
	
	// Copy to host memory
	err = cudaMemcpy(h_result, d_result, NUMBER_OF_KEYS*sizeof(DataType), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to copy vector B from device to host (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
		
	// Check_results
	/*int flag=0;
	for (int i=0;i<NUMBER_OF_KEYS;i++)
		if (correct_results[i] != h_result[i]){
		#ifdef INT
		printf("Error in key=%d (%d, %d)\n", i, correct_results[i], h_result[i]);
		#endif
		#ifdef FLOAT
		printf("Error in key=%d (%f, %f)\n", i, correct_results[i], h_result[i]);
		#endif
		#ifdef DOUBLE
		printf("Error in key=%d (%f, %f)\n", i, correct_results[i], h_result[i]);
		#endif
		flag=1;
		}
	*/
	/*if (flag == 0)
		printf("PASSED!!!\n");*/
	
	return 0;
	
}

int reduction_exploration()
{

	DataType *h_value=NULL, *h_result=NULL, *correct_results=NULL;
	int *h_key=NULL, *d_key=NULL;
	DataType *d_value=NULL, *d_result=NULL;
	cudaError_t err = cudaSuccess;
	
	double performance [3][100][100];
	
	float ttime =0, time;
	// Event creation
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
	
	int threadsPerBlock, blocksPerGrid;
	int numElements;
	
	int num_elements_base = 1024*4096;
	
	for (int coarsening=1; coarsening<10; coarsening+=2) {	
		
		//for (threadsPerBlock=32; threadsPerBlock<=1024; threadsPerBlock+=32) {
		
			for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64) {
			//printf (" ************ Coarsening = %d Blocksize = %d ************\n", coarsening, threadsPerBlock);
		
			numElements = num_elements_base * coarsening;
		
			initialize_values(&h_value, &d_value, &h_key, &d_key, &h_result, &d_result, &correct_results, numElements);
			
			/////// Reduce 6
						
			ttime=0;
			for (int iteration=0; iteration<REP+WARMUP; iteration++) {
			
				numElements = num_elements_base * coarsening;
			
				blocksPerGrid = num_elements_base /(threadsPerBlock * 2); // In each reading operation, threads read two elements
						
				send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
				cudaEventRecord( start, 0 );
		 
				global_onekey_reduce_GM<DataType><<<blocksPerGrid, threadsPerBlock, threadsPerBlock*sizeof(DataType)>>>(d_result, d_value, numElements);
				numElements = blocksPerGrid;
				
				while (numElements > 2*threadsPerBlock){ 
					blocksPerGrid = (numElements + 2*threadsPerBlock-1)/(2*threadsPerBlock);
					global_onekey_reduce_GM<DataType><<<blocksPerGrid, threadsPerBlock, threadsPerBlock * sizeof(DataType)>>>(d_result, d_result, numElements);
					numElements = blocksPerGrid;
				}
		
				global_onekey_reduce_GM<DataType><<<1, numElements/2, threadsPerBlock * sizeof(DataType)>>>(d_result, d_result, numElements);

				cudaEventRecord( stop, 0 ); 
				cudaEventSynchronize( stop );
				cudaEventElapsedTime( &time, start, stop );
		
				if (iteration >= WARMUP)
					ttime += time;
				
				if( iteration == REP+WARMUP-1) {
					performance[0][coarsening/2][(threadsPerBlock-64)/64] = (2.0*(float)(num_elements_base * coarsening*sizeof(DataType)*REP))/(ttime*1e6);
					//printf("Reduce %f %.2f\n", ttime/REP, (2.0*(float)(num_elements_base * coarsening*sizeof(DataType)*REP))/(ttime*1e6));
				
				}
				err = cudaGetLastError();
				if (err != cudaSuccess){
					fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
					exit(EXIT_FAILURE);
				}
			}
	
			get_data_and_check(h_result, d_result, correct_results);
			
			/////// NGM aggregated - GM
	
			numElements = num_elements_base * coarsening;
			blocksPerGrid = num_elements_base /(threadsPerBlock * 2); // In each reading operation, threads read two elements
			ttime=0;
			for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
				send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
				cudaEventRecord( start, 0 );
		
				global_onekey_sevvalues_aggr_NGM_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value, numElements);
		 
				cudaEventRecord( stop, 0 ); 
				cudaEventSynchronize( stop );
				cudaEventElapsedTime( &time, start, stop );
		
				if (iteration >= WARMUP)
					ttime += time;
			
				if (iteration == REP+WARMUP-1) {
					performance[1][coarsening/2][(threadsPerBlock-64)/64] = (2.0*(float)(num_elements_base * coarsening*sizeof(DataType)*REP))/(ttime*1e6);
					//printf("NGM_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
				}
				
				err = cudaGetLastError();
				if (err != cudaSuccess){
					fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
					exit(EXIT_FAILURE);
				}
			}
	
			get_data_and_check(h_result, d_result, correct_results);
			
			
			/////// NGM aggregated - SMGM
	
			numElements = num_elements_base * coarsening;
			blocksPerGrid = num_elements_base /(threadsPerBlock * 2); // In each reading operation, threads read two elements
			ttime=0;
			for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
				send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
				cudaEventRecord( start, 0 );
		
				global_onekey_sevvalues_aggr_NGM_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value, numElements);

				cudaEventRecord( stop, 0 ); 
				cudaEventSynchronize( stop );
				cudaEventElapsedTime( &time, start, stop );
		
				if (iteration >= WARMUP)
					ttime += time;
			
				if (iteration == REP+WARMUP-1){
					performance[2][coarsening/2][(threadsPerBlock-64)/64] = (2.0*(float)(num_elements_base * coarsening*sizeof(DataType)*REP))/(ttime*1e6);
				//	printf("NGM_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
				}
				
				err = cudaGetLastError();
				if (err != cudaSuccess){
					fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
					exit(EXIT_FAILURE);
				}
			}
	
			get_data_and_check(h_result, d_result, correct_results);
			
			free_values(h_value, d_value, h_key, d_key, h_result, d_result, correct_results);
		}
			
	}
	
			printf("\nReduce");
		for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
			printf("\t%d", threadsPerBlock);
		printf("\n");
		
		for (int coarsening=1; coarsening<10; coarsening+=2) {	
			printf("Coar_%d", coarsening);
			for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
				printf("\t%f", performance[0][coarsening/2][(threadsPerBlock-64)/64]);
			printf("\n");
		}
		
		printf("\nNGM_GM");
		for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
			printf("\t%d", threadsPerBlock);
		printf("\n");
		
		for (int coarsening=1; coarsening<10; coarsening+=2) {	
			printf("Coar_%d", coarsening);
			for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
				printf("\t%f", performance[1][coarsening/2][(threadsPerBlock-64)/64]);
			printf("\n");
		}
		
		printf("\nNGM_GMSM");
		for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
			printf("\t%d", threadsPerBlock);
		printf("\n");
		
		for (int coarsening=1; coarsening<10; coarsening+=2) {	
			printf("Coar_%d", coarsening);
			for (threadsPerBlock=64; threadsPerBlock<=1024; threadsPerBlock+=64)
				printf("\t%f", performance[2][coarsening/2][(threadsPerBlock-64)/64]);
			printf("\n");
		}

	return 0;

}	
		


int onekey_sevvalues_tests(int blocksPerGrid, int threadsPerBlock, int numElements)
{
	DataType *h_value=NULL, *h_result=NULL, *correct_results=NULL;
	int *h_key=NULL, *d_key=NULL;
	DataType *d_value=NULL, *d_result=NULL;
	cudaError_t err = cudaSuccess;
	
	initialize_values(&h_value, &d_value, &h_key, &d_key, &h_result, &d_result, &correct_results, numElements);
	
	float ttime =0, time;
	// Event creation
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
	
	printf("Results for one key and several values\n");
	
	/** Ojo, no parece que tenga sentido usar SM para ayudar a la reducción ya que sería mejor usar
	privatización de variable. Por tanto las pruebas deberían tener un COARSENING DE 1
	EL uso de SM si tiene sentido en las agregadas, ya que los distintos warps de un bloque harán la reducción en una posición de SM.
	De todas formas, por generalidad, se han hecho las implementaciones SM para todas las funciones**/
	
	/////// Reduce 6
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		 
		
		global_onekey_reduce_GM<DataType><<<NUM_BLOCKS/2, threadsPerBlock, threadsPerBlock*sizeof(DataType)>>>(d_result, d_value, numElements);
		//global_onekey_reduce_GM<DataType><<<1, NUM_BLOCKS/4, NUM_BLOCKS/4 * sizeof(DataType)>>>(d_result, d_result, NUM_BLOCKS/2);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("Reduce %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	//// Atomics - GM
	
	/*ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
	 
		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("Atomic_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	*/
	/////// One leader aggregated - GM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("JGL_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	/////// NGM aggregated - GM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_NGM_GM<DataType><<</*blocksPerGrid*/NUM_BLOCKS/2, threadsPerBlock>>>(d_result, d_value, NVALUE_PER_THREAD * GRID_SIZE);
		 
		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("NGM_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);

	
	/////// Westphal aggregated - GM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("West_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results); 
	
	
	
	///// Reduce 6 - SMGM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_reduce_SMGM<DataType><<<blocksPerGrid, threadsPerBlock, BS>>>(d_result, d_value, numElements);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("Reduce6_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	//// Atomics - SMGM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
	
		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("Atomic_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	
	/////// One leader aggregated - SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("JGL_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	/////// NGM aggregated - SMGM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_NGM_SMGM<DataType><<</*blocksPerGrid*/NUM_BLOCKS/2, threadsPerBlock>>>(d_result, d_value, NVALUE_PER_THREAD * GRID_SIZE);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("NGM_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);

	
	/////// Westphal aggregated - SMGM
	
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_onekey_sevvalues_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1)
			printf("West_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	
	return 0;
	
}
						
	

