#ifndef SEVERALKEYS_CUH_
#define SEVERALKEYS_CUH_

// Atomic several keys
template <typename ValType> 
__device__ inline ValType severalkeys_atomic(ValType *pos, int key, ValType val)
{
	return (atomicAdd(pos+key, val));
}

template <typename ValType> 	
__device__ inline ValType severalkeys_oneval_aggr_JGL(ValType *pos, int key, ValType val)
{

	const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

#ifdef WARPDIVERGENCE
	unsigned int vmask = __ballot(1) & mask_white; // Consider active threads belonging to this virtual warp
#else
	unsigned int vmask = 0xFFFFFFFF & mask_white; // Consider active threads belonging to this virtual warp
#endif

	unsigned int leader = __ffs(vmask)-1;

	ValType key_0 = __shfl(key, leader, VIRT_WARP_SIZE);
	
	vmask = vmask & __ballot(key_0==key);
	
	ValType sum = (ValType)__popc(vmask) * val; // Population count
	
    if(virt_lane_id() == 0)
        atomicAdd(pos+key, sum);
	else if (key_0 != key)
		atomicAdd(pos+key, val);
		
	return 0;
}
 
template <typename ValType> 
__device__ inline ValType severalkeys_severalvals_aggr_JGL(ValType *pos, int key, ValType val)
{

	const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

#ifdef WARPDIVERGENCE
	unsigned int vmask = __ballot(1) & mask_white; // Consider active threads belonging to this virtual warp
#else
	unsigned int vmask = 0xFFFFFFFF & mask_white; // Consider active threads belonging to this virtual warp
#endif

	unsigned int leader = __ffs(vmask)-1;

	int key_0 = __shfl(key, leader, VIRT_WARP_SIZE);
	
	vmask = vmask & __ballot(key_0==key);
	
	ValType res = val;
	unsigned int next = leader;
	
	while(__popc(vmask)-1 > 0){
		// Update mask
		vmask ^= (1 << next);
		next = __ffs(vmask) - 1;

		int next2 = next % VIRT_WARP_SIZE;
		res += warp_bcast(val, next2, VIRT_WARP_SIZE);	
	}
   
    if(virt_lane_id() == 0)
		atomicAdd(pos+key, res); 
	else if (key_0 != key)
		atomicAdd(pos+key, val);
		
	return 0;
}

template <typename ValType> 	
__device__ inline ValType severalkeys_oneval_aggr_Westphal(ValType *pos, int key, ValType val)
{
// Optimizado para histogramas
  bool is_peer; 
  int lane= threadIdx.x & 31; 
  uint unclaimed=__ballot(1);                   // just in case of divergence
  do {
	int leader = __ffs(unclaimed)-1;  // identify leader
    const int other_key=__shfl(key, leader); 
// get key from least unclaimed lane 
    is_peer=(key==other_key);               // do we have a match?
    int peers=__ballot(is_peer);
	ValType vote = val * (ValType)__popc(peers);
	if (lane == leader)
		atomicAdd(pos+key, vote);
                   // find all matches                 
    unclaimed^=peers;                          // matches are no longer unclaimed
  } while (!is_peer);    // repeat as long as we haven�t found our match
 

 return 0; 
}

template <typename ValType> 	
__device__ inline ValType severalkeys_severalvals_aggr_Westphal(ValType *pos, int key, ValType val)
{

   const uint peers = get_peers(key);
		
   add_peers(pos, key, val, peers);
   
   return 0;
}

template <typename ValType> 	
__device__ inline ValType severalkeys_severalvals_aggr_NGM(ValType *pos, int key, ValType val)
{

    int lane_id = threadIdx.x & 31;
      
	uint acc_mask;
	int leader = 0; 
	
	acc_mask = 0;

	while (leader >= 0){ // Process sequentially each leader

		const int k_leader = __shfl(key, leader, WARP_SIZE);
		
		const int dif = key - k_leader;
		uint mask = ~ __ballot(dif); // Threads with the same key value
	 
		ValType v1;
		if (dif == 0)
			v1=val; // value is saved
		else
			v1=0;  // v1 is the thread value for the treads with the same key value, elsewhere 0
		
		const int lsb = __ffs(mask); // least significant bit position
		const int msb = WARP_SIZE - __clz(mask); // most significant bit position
		const int distance = msb-lsb; // distance between mosy distant bits
		
		ValType vaux; // Shuffle output
		
		if (distance > 0) {
			vaux=__shfl_down(v1, 1, WARP_SIZE);
			if (lane_id < 31) // This operation avoid adding data beyond warp limits
				v1 = vaux + v1;
		
			if (distance > 1) {
				vaux=__shfl_down(v1, 2, WARP_SIZE);
				if (lane_id < 30)
					v1 = vaux + v1;

				if (distance > 3) {
					vaux=__shfl_down(v1, 4, WARP_SIZE);
					if (lane_id < 28)
						v1 = vaux + v1;

					if (distance > 7) {
						vaux=__shfl_down(v1, 8, WARP_SIZE);
						if (lane_id < 24)
							v1 = vaux + v1;
		
						if (distance > 15) {
							vaux=__shfl_down(v1, 16, WARP_SIZE);
							if (lane_id < 16)
								v1 = vaux + v1;
						}
					}
				}
			}
		}
	
		if (lane_id == leader)
			atomicAdd(&pos[key], v1);
		
		acc_mask |= mask; // Lanes already processed
	
		leader = __ffs(~acc_mask)-1; 
	}
	
	return 0;

}

#endif // SEVERAL_KEYS_CUH	
 
 
	
