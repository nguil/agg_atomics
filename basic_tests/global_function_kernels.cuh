template <typename  ValType>
__global__ void global_onekey_reduce_GM(ValType *output, ValType *input, int numElements)
{	
	int blockSize = blockDim.x;
	
	//volatile __shared__ ValType sdata[BS];
	extern __shared__ ValType sdata[];
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i<numElements){ 
		sdata[tid] += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	   
	onekey_reduction<ValType>(output, sdata, 1);
		
	return;
}

template <typename  ValType>
__global__ void global_onekey_reduce_SMGM(ValType *output, ValType *input, int numElements)
{	
	int blockSize = blockDim.x;
	
	volatile __shared__ ValType sdata[BS];
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i<numElements){ 
		sdata[tid] += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	
	onekey_reduction<ValType>(output, sdata, 1);

	if (threadIdx.x == 0)
		atomicAdd(output, sdata[0]);
		
	return;
}

template <typename ValType>
__global__ void global_onekey_atomic_GM(ValType *output, ValType *input, int numElements)
{
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	ValType val=0;
	
	while (i<numElements){ 
		val += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	
	onekey_atomic<ValType>(output, val);
		
	i += blockSize;
		
}


template <typename ValType>
__global__ void global_onekey_atomic_SMGM(ValType *output, ValType *input)
{
	
	__shared__ ValType res;
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	
	if (threadIdx.x == 0)
		res = 0;
		
	__syncthreads();
		
	onekey_atomic(&res, val);
	
	__syncthreads();
	
	if (threadIdx.x == 0)
		atomicAdd(output, res);
	
}

template <typename ValType> 
__global__ void global_onekey_aggr_filter_GM(ValType *output, ValType *input)
{
	// It cannot have SM version because GM counter content must be returned to each thread
	
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	
	onekey_oneval_aggr<ValType>(output, val);
		
	i += blockSize;
		
}

template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_oneleader_GM(ValType *output, ValType *input)
{
	
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i]; 
		i += gridSize; 
	}
	
	onekey_severalvals_aggr_JGL<ValType>(output, val);
	
}

template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_oneleader_SMGM(ValType *output, ValType *input)
{
	__shared__ ValType res;
	
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i]; 
		i += gridSize; 
	}
	
	if (threadIdx.x == 0)
		res = 0;
		
	__syncthreads();
	
	onekey_severalvals_aggr_JGL<ValType>(&res, val);
	
	__syncthreads();
	
	if (threadIdx.x == 0)
		atomicAdd(output, res);
}



template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_NGM_GM(ValType *output, ValType *input, int numElements)
{

	/*unsigned int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;*/
	unsigned int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
	ValType value=0;

	while (i<numElements){ 
		//value += input[i]; 
		value += input[i] + input[i+blockSize]; 
		i += gridSize; 
	}
	   
	onekey_severalvals_aggr_NGM<ValType>(output, value);
	
}

template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_NGM_SMGM(ValType *output, ValType *input, int numElements)
{
	__shared__ ValType res;

	/*unsigned int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;*/
	
	unsigned int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	
	ValType value=0;
	/*<NVALUE_PER_THREAD * GRID_SIZE*/
	while (i<numElements){ 
		value += input[i] + input[i+blockSize]; ; 
		i += gridSize; 
	}
	
	if (threadIdx.x == 0)
		res = 0;
		
	__syncthreads();

	onekey_severalvals_aggr_NGM<ValType>(&res, value);
		
	__syncthreads();
	
	if (threadIdx.x == 0)
		atomicAdd(output, res);
	
}

template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_Westphal_GM(ValType *output, ValType *input)
{
	
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i]; 
		i += gridSize; 
	}
	
	onekey_severalvals_aggr_Westphal<ValType>(output, val);
	
}

template <typename ValType>
__global__ void global_onekey_sevvalues_aggr_Westphal_SMGM(ValType *output, ValType *input)
{
	__shared__ ValType res;
	
	int blockSize = blockDim.x;
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize + tid;
	unsigned int gridSize = blockSize*gridDim.x;
	ValType val=0;
	
	while (i<NVALUE_PER_THREAD * GRID_SIZE){ 
		val += input[i]; 
		i += gridSize; 
	}
	
	if (threadIdx.x == 0)
		res = 0;
		
	__syncthreads();
	
	onekey_severalvals_aggr_Westphal<ValType>(&res, val);
	
	__syncthreads();
	
	if (threadIdx.x == 0)
		atomicAdd(output, res);
	
}

///// Several keys 

template <typename ValType>
__global__ void global_sevkeys_atomic_GM(ValType *output, int *key, ValType *input)
{

	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) {

		severalkeys_atomic(output, key[i], input[i]);
		
		i += blockSize;
		
	}
}

template <typename ValType>
__global__ void global_sevkeys_atomic_SMGM(ValType *output, int *key, ValType *input)
{

	__shared__ ValType smem[NUMBER_OF_KEYS];
	
	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) {

		severalkeys_atomic(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
	
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);
}

template <typename ValType>
__global__ void global_sevkeys_onevalue_aggr_oneleader_GM(ValType *output, int *key, ValType *input)
{
	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_oneval_aggr_JGL(output, key[i], input[i]);
		
		i += blockSize;
		
	}

}

template <typename ValType>
__global__ void global_sevkeys_onevalue_aggr_oneleader_SMGM(ValType *output, int *key, ValType *input)
{

	__shared__ ValType smem[NUMBER_OF_KEYS];
	
	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) {

		severalkeys_oneval_aggr_JGL(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
	
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);
}

template <typename ValType>
__global__ void global_sevkeys_severalvalues_aggr_oneleader_GM(ValType *output, int *key, ValType *input)
{
	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_JGL(output, key[i], input[i]);
		
		i += blockSize;
		
	}
	
}

template <typename ValType>
__global__ void global_sevkeys_severalvalues_aggr_oneleader_SMGM(ValType *output, int *key, ValType *input)
{
	__shared__ ValType smem[NUMBER_OF_KEYS];
	
	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_JGL(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
	
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);


}	

template <typename ValType>
__global__ void global_sevkeys_onevalue_aggr_Westphal_GM(ValType *output, int *key, ValType *input)
{
	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_oneval_aggr_Westphal(output, key[i], input[i]);
		
		i += blockSize;
		
	}

}

template <typename ValType>
__global__ void global_sevkeys_onevalue_aggr_Westphal_SMGM(ValType *output, int *key, ValType *input)
{
	__shared__ ValType smem[NUMBER_OF_KEYS];
	
	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_oneval_aggr_Westphal(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
	
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);

}

template <typename ValType>
__global__ void global_sevkeys_sevvalues_aggr_Westphal_GM(ValType *output, int *key, ValType *input)
{
	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_Westphal(output, key[i], input[i]);
		
		i += blockSize;
		
	}

}

template <typename ValType>
__global__ void global_sevkeys_sevvalues_aggr_Westphal_SMGM(ValType *output, int *key, ValType *input)
{
	__shared__ ValType smem[NUMBER_OF_KEYS];
	
	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_Westphal(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
	
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);

}

template <typename ValType>
__global__ void global_sevkeys_sevvalues_aggr_NGM_GM(ValType *output, int *key, ValType *input)
{
	int blockSize = blockDim.x;
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_NGM(output, key[i], input[i]);
		
		i += blockSize;
		
	}

}

template <typename ValType>
__global__ void global_sevkeys_sevvalues_aggr_NGM_SMGM(ValType *output, int *key, ValType *input)
{
	__shared__ ValType smem[NUMBER_OF_KEYS];

	int blockSize = blockDim.x;
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			smem[iter*blockSize+threadIdx.x]=0;
	
	__syncthreads();
	
	int i = blockIdx.x * (NVALUE_PER_THREAD * blockSize) + threadIdx.x;
	
	for (int iter = 0; iter < NVALUE_PER_THREAD; iter++) 
	{

		severalkeys_severalvals_aggr_NGM(smem, key[i], input[i]);
		
		i += blockSize;
		
	}
		
	__syncthreads();
	
	for (int iter=0; iter<(NUMBER_OF_KEYS + blockSize-1)/blockSize; iter++)
		if (iter*blockSize+threadIdx.x < NUMBER_OF_KEYS)
			atomicAdd(output+iter*blockSize+threadIdx.x, smem[iter*blockSize+threadIdx.x]);

}


