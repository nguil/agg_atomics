#ifndef BASIC_FUNCTIONS_CUH
#define BASIC_FUNCTIONS_CUH_

#define WITHOUT_DOUBLE_ATOMIC 1
#define WITHOUT_SHUFFLE 0
#define WITHOUT_DOUBLE_SHUFFLE 0

#define WARP_SIZE 32
#define VIRT_WARP_SIZE 32

#if VIRT_WARP_SIZE == 1
#define MASK 0x00000001
#elif VIRT_WARP_SIZE == 2
#define MASK 0x00000003
#elif VIRT_WARP_SIZE == 4
#define MASK 0x0000000f
#elif VIRT_WARP_SIZE == 8
#define MASK 0x000000ff
#elif VIRT_WARP_SIZE == 16
#define MASK 0x0000ffff
#else
#define MASK 0xffffffff
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////   Aggregated and Global Memory: Only one leader ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ inline int lane_id(void) { return threadIdx.x % WARP_SIZE; }
__device__ inline int virt_lane_id(void) { return threadIdx.x % VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id(void) { return threadIdx.x / VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id2(void) { return lane_id() / VIRT_WARP_SIZE; }

// Atomic add for doubles
#if WITHOUT_DOUBLE_ATOMIC
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                        __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

// Shuffle using doubles (Julien Demouth version)
#if WITHOUT_DOUBLE_SHUFFLE
__device__ __inline__ double __shfl(double x, int lane, int vw_size)
{
// Split the double number into 2 32b registers.
int lo, hi;
asm volatile( "mov.b64 {%0,%1}, %2;" : "=r"(lo), "=r"(hi) : "d"(x));
// Shuffle the two 32b registers.
 lo = __shfl(lo, lane, vw_size);
 hi = __shfl(hi, lane, vw_size);
// Recreate the 64b number.
asm volatile( "mov.b64 %0, {%1,%2};" : "=d"(x) : "r"(lo), "r"(hi));
return x;
}
#endif

// Warp broadcast
template <typename ValType>
__device__ ValType warp_bcast(ValType v, int leader, int vw_size) {
#if WITHOUT_SHUFFLE
    __shared__ volatile ValType _v[BLOCK_SIZE/VIRT_WARP_SIZE]; // One element for each virtual warp

    if ( virt_lane_id() == leader )
    	_v[virt_warp_id()] = v;

    return _v[virt_warp_id()]; // No sync, unsafe
}
#else
	return __shfl(v, leader, vw_size);
}
#endif

/****************************************************************************************/
/** Method proposed by Elmar Wetsphal: Voting and Shuffling for fewer Atomic Operations ***/
/******************************************************************************************/

__device__ __inline__ uint get_peers(const int my_key) { 
  uint peers; 
  bool is_peer; 
  uint unclaimed=0xffffffff;                   // in the beginning, no threads are claimed
  do {
    const int other_key=__shfl(my_key,__ffs(unclaimed)-1);
// get key from least unclaimed lane 
    is_peer=(my_key==other_key);               // do we have a match?
    peers=__ballot(is_peer);
                   // find all matches                 
    unclaimed^=peers;                          // matches are no longer unclaimed
  } while (!is_peer);                          // repeat as long as we haven�t found our match
  return peers; 
}

template <typename ValType> 
__device__ __inline__ ValType add_peers(ValType *dest, const int my_key, ValType x, uint peers)
 { 
  int lane= threadIdx.x & 31; 
  int first=__ffs(peers)-1;              // find the leader
  int rel_pos=__popc(peers<<(32-lane));  // find our own place
  peers&=(0xfffffffe<<lane);             // drop everything to our right
  while(__any(peers)) {                  // stay alive as long as anyone is working
    int next=__ffs(peers);               // find out what to add
    ValType t=__shfl(x,next-1);                // get what to add (undefined if nothing)
    if (next)                            // important: only add if there really is anything
      x+=t;
    int done=rel_pos&1;                  // local data was used in iteration when its LSB is set
    peers&=__ballot(!done);              // clear out all peers that were just used
    rel_pos>>=1;                         // count iterations by shifting position
  } 
  if (lane==first)                       // only leader threads for each key perform atomics 
    atomicAdd(dest+my_key,x);                   
	//ValType res=__shfl(x,first);				  // distribute result (if needed) 
  return 0;                            // may also return x or return value of atomic, as needed
} 

 

#endif  //BASIC_FUNCTIONS_CUH_
