/**
 * Host main routine
 */

#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include "test.h"
#include "basic_functions_cuda.cu"
#include "onekey_cuda.cuh"
#include "severalkeys_cuda.cuh"
#include "global_function_kernels.cuh"
#include "test_onekey_sevvalues.cu"
#include "test_sevkeys_oneval.cu"
#include "test_sevkeys_sevvalues.cu"

int main(int argc, char **argv)
{
    // Error code to check return values for CUDA calls

    // Syntax verification
    if (argc != 2) {
        printf("Wrong format\n");
        printf("Syntax: %s <Device>\n",argv[0]);
        return -1;
    }

    int device = atoi(argv[1]);
	
    // Set device
    cudaDeviceProp device_properties;
    cudaGetDeviceProperties(&device_properties,device);
    cudaSetDevice(device);
    printf("%s\n", device_properties.name);
	
	int threadsPerBlock = BS;
	int blocksPerGrid = GRID_SIZE/BS;
		
	// Print the vector length to be used, and compute its size
    int numElements = NVALUE_PER_THREAD * GRID_SIZE;
	
	//reduction_exploration();
	
	//onekey_sevvalues_tests(blocksPerGrid, threadsPerBlock, numElements);
	
	//sevkey_onevalue_tests(blocksPerGrid, threadsPerBlock, numElements);
	
	sevkey_sevvalues_tests(blocksPerGrid, threadsPerBlock, numElements);

	return 0;
}
	
/*	
    // Event creation
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    float ttime=0, time = 0;
 
	#ifdef PRINT
		printf("[Irregular reduction %d elements]\n", numElemen
	#endif

    // Allocate the host input vector A
    DataType *h_value = (DataType *)malloc(numElements*sizeof(DataType));
	int *h_key = (int *)malloc(numElements*sizeof(int));

    // Allocate the host output vectors
	DataType *h_result = (DataType *)malloc(NUMBER_OF_KEYS*sizeof(DataType));
    

    // Verify that allocations succeeded
    if (h_value == NULL || h_key == NULL || h_result == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vectors
    DataType *d_value = NULL;
    err = cudaMalloc((void **)&d_value, numElements*sizeof(DataType));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	int *d_key = NULL;
    err = cudaMalloc((void **)&d_key, numElements*sizeof(int));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
	
    // Allocate the device output vectors
	DataType *d_result = NULL;
    err = cudaMalloc((void **)&d_result, NUMBER_OF_KEYS*sizeof(DataType));
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
    }
    
	// Give values to the input vectors and calculates de correct result
	
	printf ("Number of entry elements = %d\n", numElements);
	
	DataType correct_results[NUMBER_OF_KEYS];
	for (int i=0;i<NUMBER_OF_KEYS;i++)
		correct_results[i]=0;
	for (int i=0; i<numElements; i++) {
		h_value[i] = 1; //(DataType)(i % (NUMBER_OF_KEYS+7));
		h_key[i]= i/10  % NUMBER_OF_KEYS;
		correct_results[h_key[i]]+=h_value[i];
		//h_key[i]= ((i % 32)/32 + (i/50) )% 100;
	} 
	
    // 
#ifdef PRINT
    for(int i = 0; i < numElements; ++i){
        printf("%d ",h_value[i]);
    }
    printf("\n");
#endif
	ttime = 0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {

		// Copy the host input vectors in host memory to the device input vector in device memory
		
		err = cudaMemcpy(d_value, h_value, numElements*sizeof(DataType), cudaMemcpyHostToDevice);
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	
		err = cudaMemcpy(d_key, h_key, numElements*sizeof(int), cudaMemcpyHostToDevice);
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to copy vector A from host to device (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	
		// Reset result
		err = cudaMemset(d_result, 0x0, NUMBER_OF_KEYS*sizeof(DataType));
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to initialize device counter nres (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}

		// Launch CUDA Kernel
		int threadsPerBlock = BS;
		int blocksPerGrid = GRID_SIZE/BS;
		printf("CUDA kernel launched with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);

		// Compaction using shared memory
		cudaEventRecord( start, 0 );
		
		//global_onekey_reduce_GM<DataType><<<blocksPerGrid, threadsPerBlock, BS>>>(d_result, d_value);
		//global_onekey_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_aggr_filter_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		
		//global_onekey_sevvalues_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_sevvalues_aggr_NGM_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_sevvalues_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		
		//global_onekey_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		
		//global_onekey_sevvalues_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_sevvalues_aggr_NGM_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		//global_onekey_sevvalues_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_value);
		
		//global_sevkeys_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		
		//global_sevkeys_onevalue_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_severalvalues_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_onevalue_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_sevvalues_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_sevvalues_aggr_NGM_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		
		//global_sevkeys_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_onevalue_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_severalvalues_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_onevalue_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_sevvalues_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
		//global_sevkeys_sevvalues_aggr_NGM_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
		
		if(iteration == REP+WARMUP-1)
			printf("Aggregate_atomics=%f BW=%.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
    
		// Copy to host memory
		err = cudaMemcpy(h_result, d_result, NUMBER_OF_KEYS*sizeof(DataType), cudaMemcpyDeviceToHost);
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to copy vector B from device to host (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
		
		// Check_results
		int flag=0;
		for (int i=0;i<NUMBER_OF_KEYS;i++)
			if (correct_results[i] != h_result[i]){
				#ifdef INT
				printf("Error in key=%d (%d, %d)\n", i, correct_results[i], h_result[i]);
				#endif
				#ifdef FLOAT
				printf("Error in key=%d (%f, %f)\n", i, correct_results[i], h_result[i]);
				#endif
				flag=1;
			}
		if (flag == 0)
			printf("PASSED!!!\n");
		
	}

    // Free device global memory
    err = cudaFree(d_value);
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to free device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    err = cudaFree(d_key);
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to free device counter nres (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    err = cudaFree(d_result);
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to free device vector B (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    // Free host memory
    free(h_value);
    free(h_key);
    free(h_result);
	
    return 0;
}*/