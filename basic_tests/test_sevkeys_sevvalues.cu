#include <time.h>
#define TRIES 100

int initialize_values2(DataType **h_value, DataType **d_value,  int **h_key, int **d_key, DataType **h_result, DataType **d_result, 
						DataType **correct_results, int numElements, int averagerun, int *nok)
{
	cudaError_t err = cudaSuccess;
	DataType *lh_value, *ld_value, *lh_result, *ld_result, *lcorrect_results;
	int *lh_key, *ld_key;
	
    // Allocate the host input vector A
    lh_value = (DataType *)malloc(numElements*sizeof(DataType));
	lh_key = (int *)malloc(numElements*sizeof(int));

    // Allocate the host output vectors
	lh_result = (DataType *)malloc(numElements*sizeof(DataType));

    // Verify that allocations succeeded
    if (lh_value == NULL || lh_key == NULL || lh_result == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vectors
    ld_value = NULL;
    err = cudaMalloc((void **)&ld_value, numElements*sizeof(DataType));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	ld_key = NULL;
    err = cudaMalloc((void **)&ld_key, numElements*sizeof(int));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
	
    // Allocate the device output vectors
	ld_result = NULL;
    err = cudaMalloc((void **)&ld_result, numElements*sizeof(DataType));
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
    }
    
	// Give values to the input vectors and calculates de correct result
	
	//printf ("Number of entry elements = %d\n", numElements);
	
	srand(time(NULL));
	int key =0;
	int cont =0;
	
	while ( cont < numElements){
		
		int next_run = rand() % (2* averagerun);
			
		for (int i=0; i < next_run && cont < numElements; i++){
			lh_key[cont]=key;
			lh_value[cont]=(DataType)1;
			cont++;
		}
		
		key++;
	}
	
	int number_of_keys = key; 
	
	lcorrect_results = (DataType *)calloc(number_of_keys, sizeof(DataType));
	for (int i=0; i<numElements; i++) {
		lcorrect_results[lh_key[i]] += lh_value[i];
	}
	
	*h_value = lh_value;
	*d_value = ld_value;
	*h_key = lh_key;
	*d_key = ld_key;
	*h_result = lh_result;
	*d_result = ld_result;
	*correct_results = lcorrect_results;
	*nok = number_of_keys;
	
	return 0;
	
}

int get_data_and_check2(DataType *h_result, DataType *d_result, DataType *correct_results, int number_of_keys)
{
	cudaError_t err = cudaSuccess;
	
	// Copy to host memory
	err = cudaMemcpy(h_result, d_result, number_of_keys*sizeof(DataType), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to copy vector B from device to host (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE); 
	}
		   
	// Check_results
	int flag=0;
	for (int i=0;i<NUMBER_OF_KEYS;i++)
		if (correct_results[i] != h_result[i]){
		#ifdef INT
		printf("Error in key=%d (%d, %d)\n", i, correct_results[i], h_result[i]);
		#endif
		#ifdef FLOAT
		printf("Error in key=%d (%f, %f)\n", i, correct_results[i], h_result[i]);
		#endif
		#ifdef DOUBLE
		printf("Error in key=%d (%f, %f)\n", i, correct_results[i], h_result[i]);
		#endif
		flag=1;
		}
	
	/*if (flag == 0) 
	{
		printf("PASSED!!!\n");
	}
	*/
	return 0;
	
}
  
int sevkey_sevvalues_tests(int blocksPerGrid, int threadsPerBlock, int numElements)
{
	DataType *h_value=NULL, *h_result=NULL, *correct_results=NULL;
	int *h_key=NULL, *d_key=NULL;
	DataType *d_value=NULL, *d_result=NULL;
	cudaError_t err = cudaSuccess;
	
	int number_of_keys;
	
	float ttime[4], time;
	// Event creation
    cudaEvent_t start, stop;
    cudaEventCreate(&start); 
    cudaEventCreate(&stop);
	
	printf("Results for several keys and several values\n");
	
	/*for (int degeneration=1, cont=0; degeneration<=512; degeneration = degeneration*2, cont++){
	
		memset(correct_results, 0, NUMBER_OF_KEYS*sizeof(DataType));
		for (int i=0; i<numElements; i++) {
			h_value[i] = (DataType)(i % (NUMBER_OF_KEYS+7));
			h_key[i]= (i/degeneration)  % NUMBER_OF_KEYS;
			correct_results[h_key[i]] += h_value[i];
		}
		
		printf("*\n****Degeneration = %d\n", degeneration);
	*/
	
	
	// Warm-up
	int averagerun=20;
	initialize_values2(&h_value, &d_value, &h_key, &d_key, &h_result, &d_result, &correct_results, numElements, averagerun, &number_of_keys);
	for (int iteration=0; iteration< WARMUP; iteration++) {
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		global_sevkeys_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);
	}			
	free_values(h_value, d_value, h_key, d_key, h_result, d_result, correct_results);

	printf("Performance of segreduction in GB/s BS=%d NUM_BLOCKS=%d COARSENING=%d\n", BS, NUM_BLOCKS, NVALUE_PER_THREAD);
	
	printf("AvgRun\tAtom_GM\tJGL_GM\tNGM_GM\tWest_GM \n"); 
	
	for (int averagerun=10, cont=0; averagerun<=500; averagerun+=40, cont++)
	{
		
	ttime[0] = 0;
	ttime[1] = 0;
	ttime[2] = 0;
	ttime[3] = 0;
		
		
///// Atomics GM
	for (int iteration=0; iteration<REP; iteration++) {
	
		initialize_values2(&h_value, &d_value, &h_key, &d_key, &h_result, &d_result, &correct_results, numElements, averagerun, &number_of_keys);
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		ttime[0] += time;
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	
		get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
		
		if(iteration == REP-1) {
			printf("%d\t%.3f", averagerun, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime[0]*1e6));
		}
	
	///// JGL GM
					
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_severalvalues_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		ttime[1] += time;
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
		
		get_data_and_check2(h_result, d_result, correct_results, number_of_keys);

		if(iteration == REP-1){
			printf("\t%.3f", (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime[1]*1e6));
		}
	
	///// NGM GM
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_NGM_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		ttime[2] += time;
					
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
		
		if(iteration == REP-1){
			printf("\t%.3f", (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime[2]*1e6));
		}
	
		get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
	
	/////  Westphal GM
							
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		ttime[3] += time;
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	
		get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
		
		if(iteration == REP-1){
			printf("\t%.3f\n", (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime[3]*1e6));
		}
		
	/*
	///// Atomics SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("Atomics_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[4][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
	
	///// JGL SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_severalvalues_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("JGL_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[5][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
	
	///// NGM SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_NGM_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("NGM_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[6][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check2(h_result, d_result, correct_results, number_of_keys);

	
	/////  Westphal SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) { 
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("West_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[7][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check2(h_result, d_result, correct_results, number_of_keys);
	*/
	
	free_values(h_value, d_value, h_key, d_key, h_result, d_result, correct_results);

	}

	
	}
	
	//printf("Method deg1 deg2 deg4 deg8 deg16 deg32 deg64 deg128 deg512\n");
	
	/*printf("\nAtomics_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[0][i]);
	printf("\nJGL_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[1][i]);
	printf("\nNGM_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[2][i]);
	printf("\nWest_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[3][i]);	
	printf("\nAtomics_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[4][i]);
	printf("\nJGL_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[5][i]);
	printf("\nNGM_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[6][i]);
	printf("\nWest_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[7][i]);*/
	return 0;
}
	
	