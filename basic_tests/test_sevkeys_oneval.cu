
int initialize_values_histo(DataType **h_value, DataType **d_value,  int **h_key, int **d_key, DataType **h_result, DataType **d_result, 
						DataType **correct_results, int numElements)
{
	cudaError_t err = cudaSuccess;
	DataType *lh_value, *ld_value, *lh_result, *ld_result, *lcorrect_results;
	int *lh_key, *ld_key;
	
    // Allocate the host input vector A
    lh_value = (DataType *)malloc(numElements*sizeof(DataType));
	lh_key = (int *)malloc(numElements*sizeof(int));

    // Allocate the host output vectors
	lh_result = (DataType *)malloc(NUMBER_OF_KEYS*sizeof(DataType));

    // Verify that allocations succeeded
    if (lh_value == NULL || lh_key == NULL || lh_result == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }

    // Allocate the device input vectors
    ld_value = NULL;
    err = cudaMalloc((void **)&ld_value, numElements*sizeof(DataType));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	ld_key = NULL;
    err = cudaMalloc((void **)&ld_key, numElements*sizeof(int));
    if (err != cudaSuccess){
        fprintf(stderr, "Failed to allocate device vector A (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
	
    // Allocate the device output vectors
	ld_result = NULL;
    err = cudaMalloc((void **)&ld_result, NUMBER_OF_KEYS*sizeof(DataType));
	if (err != cudaSuccess){
		fprintf(stderr, "Failed to allocate device vector B (error code %s)!\n", cudaGetErrorString(err));
		exit(EXIT_FAILURE);
    }
    
	// Give values to the input vectors and calculates de correct result
	
	printf ("Number of entry elements = %d\n", numElements);
	lcorrect_results = (DataType *)calloc(NUMBER_OF_KEYS, sizeof(DataType));
	for (int i=0; i<numElements; i++) {
		lh_value[i] = (DataType) 1; //(DataType)(i % (NUMBER_OF_KEYS+7));
		lh_key[i]= (i/DEGENERATION)  % NUMBER_OF_KEYS;
		lcorrect_results[lh_key[i]] += lh_value[i];
	}
	
	*h_value = lh_value;
	*d_value = ld_value;
	*h_key = lh_key;
	*d_key = ld_key;
	*h_result = lh_result;
	*d_result = ld_result;
	*correct_results = lcorrect_results;
	
	return 0;
	
}

int sevkey_onevalue_tests(int blocksPerGrid, int threadsPerBlock, int numElements)
{
	DataType *h_value=NULL, *h_result=NULL, *correct_results=NULL;
	int *h_key=NULL, *d_key=NULL;
	DataType *d_value=NULL, *d_result=NULL;
	cudaError_t err = cudaSuccess;
	
	float store_times[100][100];
	
	initialize_values_histo(&h_value, &d_value, &h_key, &d_key, &h_result, &d_result, &correct_results, numElements);
	
	float ttime =0, time;
	// Event creation
    cudaEvent_t start, stop;
    cudaEventCreate(&start); 
    cudaEventCreate(&stop);
	
	printf("Results for several keys and one values\n");
	
	for (int degeneration=1, cont=0; degeneration<=512; degeneration = degeneration*2, cont++){
	
		for (int i=0; i<numElements; i++) {
			h_value[i] = (DataType) 1; //(DataType)(i % (NUMBER_OF_KEYS+7));
			h_key[i]= (i/degeneration)  % NUMBER_OF_KEYS;
			correct_results[h_key[i]] += h_value[i];
		}
		
		printf("*\n****Degeneration = %d\n", degeneration);
	
	///// Atomics GM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_atomic_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1) {
			printf("Atomics_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
    
			store_times[0][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	///// JGL GM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_onevalue_aggr_oneleader_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("JGL_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));

			store_times[1][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	///// NGM GM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_NGM_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("NGM_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[2][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	/////  Westphal GM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_onevalue_aggr_Westphal_GM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("West_GM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[3][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	///// Atomics SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_atomic_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("Atomics_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[4][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	///// JGL SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_onevalue_aggr_oneleader_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("JGL_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[5][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	///// NGM SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_sevvalues_aggr_NGM_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("NGM_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[6][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	/////  Westphal SMGM
	ttime=0;
	for (int iteration=0; iteration<REP+WARMUP; iteration++) {
						
		send_data_to_device(h_value, d_value, h_key, d_key, d_result, numElements);
		cudaEventRecord( start, 0 );
		
		global_sevkeys_onevalue_aggr_Westphal_SMGM<DataType><<<blocksPerGrid, threadsPerBlock>>>(d_result, d_key, d_value);

		cudaEventRecord( stop, 0 ); 
		cudaEventSynchronize( stop );
		cudaEventElapsedTime( &time, start, stop );
		
		if (iteration >= WARMUP)
			ttime += time;
			
		if(iteration == REP+WARMUP-1){
			printf("West_SMGM %f %.2f\n", ttime/REP, (2.0*(float)(numElements*sizeof(DataType)*REP))/(ttime*1e6));
			store_times[7][cont]= ttime/REP;
		}
		
		err = cudaGetLastError();
		if (err != cudaSuccess){
			fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	}
	
	get_data_and_check(h_result, d_result, correct_results);
	
	}
	
	printf("Method deg1 deg2 deg4 deg8 deg16 deg32 deg64 deg128 deg512\n");
	
	printf("\nAtomics_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[0][i]);
	printf("\nJGL_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[1][i]);
	printf("\nNGM_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[2][i]);
	printf("\nWest_GM ");for (int i=0;i<10;i++) printf("%f ", store_times[3][i]);	
	printf("\nAtomics_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[4][i]);
	printf("\nJGL_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[5][i]);
	printf("\nNGM_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[6][i]);
	printf("\nWest_SMGM ");for (int i=0;i<10;i++) printf("%f ", store_times[7][i]);
	return 0;
}
	
	
	
	