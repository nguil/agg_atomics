#ifndef TEST_CUH_
#define TEST_CUH_

//#define PRINT  
#define DOUBLE// OPTIONS: INT FLOAT DOUBLE

#ifdef INT
typedef int DataType ;
#endif
#ifdef FLOAT
typedef float DataType ;
#endif 
#ifdef DOUBLE
typedef double DataType ; 
#endif

#define WARP_SIZE 32


#define BS 128
#define NUM_BLOCKS 8192
#define GRID_SIZE BS*NUM_BLOCKS

#define REP 100
#define WARMUP 3

#define NVALUE_PER_THREAD 10

#define NUMBER_OF_KEYS 1
#define DEGENERATION 4  // Histograms: number of similar consecutive keys

		
#endif
