#ifndef AGGRGLOBAL_CUH_
#define AGGRGLOBAL_CUH_

// Reduction one key 
template <typename ValType>
__device__ inline ValType onekey_reduction(ValType *pos, volatile ValType *sdata, bool participate)
{
	unsigned int tid = threadIdx.x;
	int blockSize = blockDim.x;
	  
	if (!participate)
		sdata[tid] = 0;
	
	__syncthreads();
	
	if (blockSize >=1024) { if (tid < 512) { sdata[tid] += sdata[tid + 512]; } __syncthreads(); }
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid < 64) { sdata[tid] += sdata[tid + 64]; } __syncthreads(); }
	
	if (tid < 32) {//warpReduce(sdata, tid);
	
		if (blockSize >= 64) sdata[tid] += sdata[tid + 32];
		if (blockSize >= 32) sdata[tid] += sdata[tid + 16];
		if (blockSize >= 16) sdata[tid] += sdata[tid + 8];
		if (blockSize >= 8) sdata[tid] += sdata[tid + 4];
		if (blockSize >= 4) sdata[tid] += sdata[tid + 2];
		if (blockSize >= 2) sdata[tid] += sdata[tid + 1];
	}

	if (tid == 0) pos[blockIdx.x] = sdata[0]; 
	
	//f (tid == 0) atomicAdd(pos, sdata[0]);
	
}


// Atomic one key
template <typename ValType> 
__device__ inline ValType onekey_atomic(ValType *pos, ValType val)
{
	return (atomicAdd(pos, val));
}

// Aggregated filtering: one key and one value
template <typename ValType> 	
__device__ inline ValType onekey_oneval_aggr(ValType *pos, ValType val)
{

#ifdef WARPDIVERGENCE
	unsigned int mask = (unsigned int)__ballot(1);
	int leader = __ffs(mask -1);
#else
	unsigned int leader = 0;
	unsigned int mask = 0XFFFFFFFF;
#endif

    unsigned int elem = __popc(mask); // Population count
	
	ValType res;
	ValType sum = (ValType)elem * val;
	if (lane_id() == leader)
		res = atomicAdd(pos, sum);
		
	res = warp_bcast<ValType>(sum, leader, VIRT_WARP_SIZE);
	
	ValType sal = res + val * (ValType) (__popc(mask & ((1 << lane_id() ) - 1)));
	
	return sal;
}

// Aggregated filtering: one key and one value
template <typename ValType> 	
__device__ inline ValType onekey_severalvals_aggr_JGL(ValType *pos, ValType val)
{

const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

#ifdef WARPDIVERGENCE
	unsigned int vmask = __ballot(1) & mask_white; // Consider active threads belonging to this virtual warp
#else
	unsigned int vmask = 0xFFFFFFFF & mask_white; // Consider active threads belonging to this virtual warp
#endif

	unsigned int leader = __ffs(vmask)-1;

	ValType res = val;
	unsigned int next = leader;
	
	while(__popc(vmask)-1 > 0){
		// Update mask
		vmask ^= (1 << next);
		next = __ffs(vmask) - 1;

		int next2 = next % VIRT_WARP_SIZE;
		res += warp_bcast(val, next2, VIRT_WARP_SIZE);	
	}
   
    if(lane_id() == leader)
		atomicAdd(pos, res); 
                        
	return 0; // Ojo, habr�a que devolver el valor como lo har�a la at�mica
}

template <typename ValType> 	
__device__ inline ValType onekey_severalvals_aggr_NGM(ValType *pos, ValType val)
{

	ValType v1;
	int ln_id = lane_id();
	
#ifdef WARPDIVERGENCE

	unsigned int mask = __ballot(1)  // Consider active threads 
	int leader = 0;
	if (mask & (1 << ln_id))
		v1 = val;
	else
		v1 = 0;
	
#else
	int leader = 0;
	v1 = val; // Consider active threads belonging to this virtual warp
	//int distance = 31;
#endif

	ValType vaux; // Shuffle output
	/*
	if (distance > 0) {
		vaux=__shfl_down(v1, 1, WARP_SIZE);
		if (ln_id< 31) // This operation avoid adding data beyond warp limits
			v1 = vaux + v1;
		
		if (distance > 1) {
			vaux=__shfl_down(v1, 2, WARP_SIZE);
			if (ln_id < 30)
				v1 = vaux + v1;

			if (distance > 3) {
				vaux=__shfl_down(v1, 4, WARP_SIZE);
				if (ln_id < 28)
					v1 = vaux + v1;

				if (distance > 7) {
					vaux=__shfl_down(v1, 8, WARP_SIZE);
					if (ln_id < 24)
						v1 = vaux + v1;
		
					if (distance > 15) {
						vaux=__shfl_down(v1, 16, WARP_SIZE);
						if (ln_id< 16)
							v1 = vaux + v1;
					}
				}
			}
		}
	}
*/
		
	vaux=__shfl_down(v1, 1, WARP_SIZE);
	//if (ln_id % 2 == 0)
	v1 = v1 + vaux;
		
	vaux=__shfl_down(v1, 2, WARP_SIZE);
	//if (ln_id % 4 == 0) 
	v1 = v1 + vaux;
		
	vaux=__shfl_down(v1, 4, WARP_SIZE);
	//if (ln_id % 8 == 0) 
	v1 = v1 + vaux;
		
	vaux=__shfl_down(v1, 8, WARP_SIZE);
	//if (ln_id % 16 == 0)
	v1 = v1 + vaux;
		
	vaux=__shfl_down(v1, 16, WARP_SIZE);
	//if (ln_id == 0) 
	v1 = v1 + vaux;
		
	if (ln_id == leader)
			atomicAdd(pos, v1);
			
	return 0;
} 

template <typename ValType> 	
__device__ inline ValType onekey_severalvals_aggr_Westphal(ValType *pos, ValType val)
{

#ifdef WARPDIVERGENCE
	unsigned int peers = __ballot(1);
#else
	unsigned int peers = 0xFFFFFFFF;
#endif

	add_peers(pos, 0, val, peers);
	
	return 0;
}

	
// Aggregated Juan several keys and one value
template <typename ValType> 	
__device__ inline ValType severalkeys_oneval_aggr(ValType *pos, unsigned int key, ValType val)
{

	const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

#ifdef WARPDIVERGENCE
	unsigned int vmask = __ballot(1) & mask_white; // Consider active threads belonging to this virtual warp
#else
	unsigned int vmask = 0xFFFFFFFF & mask_white; // Consider active threads belonging to this virtual warp
#endif

	unsigned int leader = __ffs(vmask -1);
		
	unsigned int key_0 = warp_bcast(key, leader, VIRT_WARP_SIZE); // Leader value 
    unsigned int ball_0 = (unsigned int)__ballot(key_0 == key); // Bit N=1 if pred=true for thread N 
    unsigned int ball_1 = ball_0 & mask_white;   // Mask according to virtual warp size
    unsigned int sum = (unsigned int)__popc(ball_1); // Population count
	
    if(virt_lane_id() == leader)
		atomicAdd(pos+key, val * sum); 
     
        else if( key_0 != key)
            atomicAdd(pos+key, val); 
                        
	return 0; // Ojo, habr�a que devolver el valor como lo har�a la at�mica
}

// Aggregated several keys and several values
template <typename ValType> 	
__device__ inline ValType severalkeys_severalvals_aggr(ValType *pos, unsigned int key, ValType val)
{

	const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

#ifdef WARPDIVERGENCE
	unsigned int vmask = __ballot(1) & mask_white; // Consider active threads belonging to this virtual warp
#else
	unsigned int vmask = 0xFFFFFFFFFF & mask_white; // Consider active threads belonging to this virtual warp
#endif

	unsigned int leader = __ffs(vmask -1);
	unsigned int key_0 = warp_bcast(key, leader, VIRT_WARP_SIZE); // Leader value 


	ValType res = val;
	unsigned int next = leader;

   while(__popc(vmask)-1 > 0){
	// Update mask
	vmask ^= (1 << next);
	next = __ffs(vmask) - 1;

	int next2 = next % VIRT_WARP_SIZE;
	res += warp_bcast(val, next2, VIRT_WARP_SIZE);	
	}
   
    if(virt_lane_id() == leader)
		atomicAdd(pos+key, res); 
     
        else if( key_0 != key)
            atomicAdd(pos+key, val); 
                        
	return 0; // Ojo, habr�a que devolver el valor como lo har�a la at�mica
}


#endif /* AGGRGLOBAL_CUH_ */

