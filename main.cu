#include <fstream>
#include <iostream>
#include <vector>
#include <string>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <cusp/io/matrix_market.h>
#include <cusp/coo_matrix.h>
#include <cusp/hyb_matrix.h>
#include <cusp/csr_matrix.h>
#include <cusp/dia_matrix.h>
#include <cusp/ell_matrix.h>
#include <cusp/multiply.h>
#include <cusp/array2d.h>
#include <cusp/print.h>
#include <thrust/device_ptr.h>

#define BLOCK_SIZE 256
#define GRANULARITY 10
#define SM_KEYS 1024

//#include <generics/shfl.h>
#include "AggrGlobal.cuh"

using namespace std;

const int NumThreadsPerBlock = BLOCK_SIZE;

// This routine assumes a COO format matrix
template <typename IndexType,
		  typename ValueType>
__global__ void coo_atomic_spmv(const ValueType *Aval,
		const IndexType *ARow,
		const IndexType *ACol,
		const ValueType *b,
		ValueType *y,
		const IndexType num_nonzeros)
{
	for (int iter=0; iter<GRANULARITY; iter++) {
	
	const IndexType idx = iter* gridDim.x * blockDim.x + blockIdx.x*blockDim.x+threadIdx.x;
	if ( idx < num_nonzeros )
	{
		const IndexType rowidx = ARow[idx];
		const IndexType colidx = ACol[idx];
		const ValueType value = Aval[idx]*b[colidx];
		atomicAdd(&y[rowidx], value);
	}
	}
}

template <typename Matrix,
         typename Vector1,
         typename Vector2>
void host_coo_atomic_spmv(const Matrix& A,
		const Vector1& b,
		Vector2& y,
		int agg)
{
	typedef typename Matrix::index_type IndexType;
    typedef typename Matrix::value_type ValueType;

    const IndexType * ACol = thrust::raw_pointer_cast(&A.column_indices[0]);
    const ValueType * Aval =  thrust::raw_pointer_cast(&A.values[0]);
    const IndexType * ARow = thrust::raw_pointer_cast(&A.row_indices[0]);

   	dim3 gridDim(((A.num_entries+GRANULARITY-1)/GRANULARITY+NumThreadsPerBlock-1)/NumThreadsPerBlock);
    dim3 blockDim(NumThreadsPerBlock);
	
	/// Methods using global memory

    if ( agg == 0 )
    	coo_atomic_spmv<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);
    if (agg == 1) 
		coo_agg_atomic_spmv<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);
	if (agg == 2)
		coo_agg_ireduction_GM<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);
	if (agg == 3)
		coo_Westphal_atomics_GM<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);
	if (agg == 4)
		SMGM_coo_agg_atomic_spmv_ver2<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);	
	if (agg == 5)
		SMGM_coo_atomic_spmv<IndexType, ValueType> <<<gridDim, blockDim>>> (Aval, ARow, ACol, thrust::raw_pointer_cast(&b[0]), thrust::raw_pointer_cast(&y[0]), A.num_entries);	
}

vector<string> GetMatrixFilenames(string path_to_matrices)
{
	string filepath;
	DIR *dp, *subdp;
	struct dirent *dirp, *subdirp;
	struct stat filestat;
	vector<string> matrix_files;

	dp = opendir( path_to_matrices.c_str() );
	if (dp == NULL)
	{
		cout << "Error opening " << path_to_matrices << endl;
		return matrix_files;
	}

	while ((dirp = readdir( dp )))
	{
		filepath = path_to_matrices + "/" + dirp->d_name;
		// If file is invalid, skip it
		if (stat( filepath.c_str(), &filestat )) continue;
	    // If the file is a directory we look further for a matrix
	    if ( S_ISDIR( filestat.st_mode ))
	    {
	    	subdp = opendir( filepath.c_str() );
	    	if (subdp == NULL)
	    	{
	    		cout << "Error opening " << filepath << endl;
	    		continue;
	    	}
	    	while ((subdirp = readdir( subdp )))
	    	{
	    		string morefiles = filepath + "/" + subdirp->d_name;
	    		string extension;
	    		if ( morefiles.find_last_of(".") != std::string::npos )
	    		{
	    			extension = morefiles.substr(morefiles.find_last_of(".") + 1);
	    			if ( extension.compare("mtx") == 0 )
	    			{
	    				matrix_files.push_back(morefiles.c_str());
	    			}

	    		}
	    	}
	    	closedir(subdp);
	    }
    }
	closedir( dp );

	return matrix_files;
}

int main(void)
{
    ////////////////////////////////////////////////////////////////////////
	// Initialization
	////////////////////////////////////////////////////////////////////////

	typedef double TipoUsadoCUSP;
	typedef double TipoUsadoAtomics;
	TipoUsadoCUSP zero_value_cusp = 0.0;
	TipoUsadoAtomics zero_value_atom = 0.0;

	ofstream results;
	ifstream fin;
	vector<string> matrix_files = GetMatrixFilenames("./nuevo_data");

	results.open("results.txt");

	int deviceCount = 0;
	cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
	if (error_id != cudaSuccess)
	{
		printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
		printf("Result = FAIL\n");
		exit(EXIT_FAILURE);
	}

	int dev;
    cudaDeviceProp deviceProp;
    for ( dev = 0; dev < deviceCount; ++dev)
    {
        cudaSetDevice(dev);
        cudaGetDeviceProperties(&deviceProp, dev);

        printf("Device %d: \"%s\"\t", dev, deviceProp.name);
        printf("  CUDA Capability:    %d.%d\n", deviceProp.major, deviceProp.minor);
    }
    cudaSetDevice(1);
    cudaGetDeviceProperties(&deviceProp, dev);
	results << "Device " << deviceProp.name << std::endl;

	//
	// load a matrix_market format matrix from disk into a coo_matrix
	//
	cusp::csr_matrix<int, TipoUsadoCUSP, cusp::host_memory> hA;
	results << "Matrix\tRows\tColumns\tEntries\tELL\tHybrid\tCSR\tCOO\tGlobal_Non_Agg\tGlobal_Agg\tGlobal_Irr\tGlobal_West\tGMSM_Agg\tGMSM";
	results << "\n";
	for ( int matrix_idx = 0; matrix_idx < matrix_files.size(); matrix_idx++)
	{
		cout << matrix_files[matrix_idx] << endl;

		cusp::io::read_matrix_market_file(hA, matrix_files[matrix_idx].c_str());

		results << matrix_files[matrix_idx].c_str();

		//
		// initialize input vector
		//
		size_t n = hA.num_cols;
		cusp::array1d<TipoUsadoCUSP, cusp::host_memory> hx(n);
		for (int i=0; i<n; i++) hx[i] = (TipoUsadoCUSP) /*(i%2+0.5); */(i%2);// - (i+1)%2; // To reduce overflow errors

		results << "\t" << hA.num_rows << "\t" << hA.num_cols << "\t" << hA.num_entries;

    ////////////////////////////////////////////////////////////////////////
    // CUSP
    ////////////////////////////////////////////////////////////////////////

	//
	// Convert and transfer to device the matrices. Use coo, csr, dia, ell or hyb to select matrix format
	//
    // allocate output vector

    cudaEvent_t start, stop;
    float time, mintime, acctime = 0;
    int nTrials = 10;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cusp::array1d<TipoUsadoCUSP, cusp::device_memory> dx(hx);
    cusp::array1d<TipoUsadoCUSP, cusp::device_memory> ycoo(n);

    float fill_ratio = 5; //float(cusp::compute_max_entries_per_row(hA))*float(hA.num_rows)/float(hA.num_entries);
    if ( fill_ratio < 3 && hA.num_entries > 1.0e06 )
    {
    	cusp::ell_matrix<int, TipoUsadoCUSP, cusp::device_memory> dA(hA);

    	// compute y = A * x

    	mintime = 1000000000;
    	for ( int i = 0; i < nTrials; i++ )
    	{
    		cusp::blas::fill(ycoo, zero_value_cusp);
    		cusp::convert(hA, dA);
    		cusp::convert(hx, dx);
    		cudaEventRecord(start, 0);
    		cusp::multiply(dA, dx, ycoo);
    		cudaEventRecord(stop, 0);
    		cudaEventSynchronize(stop);
    		cudaEventElapsedTime(&time, start, stop);
    		if ( time < mintime ) mintime = time;
    		acctime += time;
    	}
//    	printf ("Time for CUSP ELL format: %f (min %f) ms\n", acctime/nTrials, mintime);
    	results << "\t" << mintime;
    }
    else
    {
    	printf("CUSP ELL format not done because fill ratio = %f\n", fill_ratio);
    	cusp::blas::fill(ycoo, zero_value_cusp);
    	results << "\t" << 999999;
    }

    cusp::array1d<TipoUsadoCUSP, cusp::host_memory> hyell(ycoo);

	cusp::hyb_matrix<int, TipoUsadoCUSP, cusp::device_memory> dAhyb(hA);
    mintime = 1000000000;
    acctime = 0;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(ycoo, zero_value_cusp);
    	cudaEventRecord(start, 0);
    	cusp::multiply(dAhyb, dx, ycoo);
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

//    printf ("Time for CUSP hybrid format: %f (min %f) ms\n", acctime/nTrials, mintime);
	results << "\t" << mintime;
    cusp::array1d<TipoUsadoCUSP, cusp::host_memory> hyhyb(ycoo);

	cusp::csr_matrix<int, TipoUsadoCUSP, cusp::device_memory> dAcsr(hA);
    mintime = 1000000000;
    acctime = 0;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(ycoo, zero_value_cusp);
        cudaEventRecord(start, 0);
    	cusp::multiply(dAcsr, dx, ycoo);
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

    cusp::array1d<TipoUsadoCUSP, cusp::host_memory> hycsr(ycoo);
    // COO format
	results << "\t" << mintime;

	cusp::coo_matrix<int, TipoUsadoCUSP, cusp::device_memory> dAcoo(hA);
    mintime = 1000000000;
    acctime = 0;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(ycoo, zero_value_cusp);
        cudaEventRecord(start, 0);
    	cusp::multiply(dAcoo, dx, ycoo);
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

    cusp::array1d<TipoUsadoCUSP, cusp::host_memory> hycoo(ycoo);
	results << "\t" << mintime;

    ////////////////////////////////////////////////////////////////////////
    // Atomics
    ////////////////////////////////////////////////////////////////////////

	//
	// Convert and transfer the matrices to the device
	//
    //cusp::csr_matrix<int, TipoUsadoAtomics, cusp::host_memory> hAatom(hA);
    cusp::array1d<TipoUsadoAtomics, cusp::device_memory> dxatom(hx);

    // allocate output vector
    cusp::array1d<TipoUsadoAtomics, cusp::device_memory> yatom(n);

    cusp::coo_matrix<int, TipoUsadoAtomics, cusp::device_memory> dAglobalatom(hA);

    // compute y = A * x using atomics
    acctime = 0;
    mintime = 1000000000;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 0 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

	results << "\t" << mintime;
    cusp::array1d<TipoUsadoAtomics, cusp::host_memory> hyglobalatom(yatom);

    // compute y = A * x using atomics
    acctime = 0;
    mintime = 1000000000;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 1 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }
	
	results << "\t" << mintime;
    cusp::array1d<TipoUsadoAtomics, cusp::host_memory> hyglobalaggatom(yatom);
	
	// compute y = A * x using atomics
    acctime = 0;
    mintime = 1000000000;
    for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 2 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

	results << "\t" << mintime;
	cusp::array1d<TipoUsadoAtomics, cusp::host_memory> irglobalaggatom(yatom);
	
	acctime = 0;
    mintime = 1000000000;
	for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 3 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

	results << "\t" << mintime;
	cusp::array1d<TipoUsadoAtomics, cusp::host_memory> Westglobalaggatom(yatom);
	
	acctime = 0;
    mintime = 1000000000;
	for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 4 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

	results << "\t" << mintime;
	cusp::array1d<TipoUsadoAtomics, cusp::host_memory> gmsmaggatom(yatom);
	
	acctime = 0;
    mintime = 1000000000;
	for ( int i = 0; i < nTrials; i++ )
    {
        cusp::blas::fill(yatom, zero_value_atom);
    	cudaEventRecord(start, 0);
        host_coo_atomic_spmv( dAglobalatom, dxatom, yatom, 5 );
    	cudaEventRecord(stop, 0);
    	cudaEventSynchronize(stop);
        cudaEventElapsedTime(&time, start, stop);
        if ( time < mintime ) mintime = time;
        acctime += time;
    }

	results << "\t" << mintime;
	cusp::array1d<TipoUsadoAtomics, cusp::host_memory> gmsmatom(yatom);

    ////////////////////////////////////////////////////////////////////////
    // Check results
    ////////////////////////////////////////////////////////////////////////

    double errorH = 0, errorO = 0, errorG = 0, errorGagg = 0, errorirGagg = 0, errorWestGagg=0, errorC = 0, errorgmsmAgg=0, errorgmsm=0;
    for (int i = 0; i < n; i++)
    {
    	errorH += abs(hycsr[i] - hyhyb[i]);
    	errorC += abs(hycsr[i] - hyell[i]);
    	errorO += abs(hycsr[i] - hycoo[i]);
    	errorG += abs(hycsr[i] - hyglobalatom[i]);
    	errorGagg += abs(hycsr[i] - hyglobalaggatom[i]);
		errorirGagg += abs(hycsr[i] - irglobalaggatom[i]);
		errorWestGagg += abs(hycsr[i] - Westglobalaggatom[i]);
		errorgmsmAgg += abs(hycsr[i] - gmsmaggatom[i]);
		if (abs(hycsr[i] - gmsmaggatom[i])>0.5)
			printf("Aqui\n");
		errorgmsm += abs(hycsr[i] - gmsmatom[i]);
    }

    double maxError = 0.000001*n;
    if ( errorH > maxError || errorO > maxError || errorGagg > maxError || errorG > maxError || 
		errorirGagg > maxError || errorWestGagg > maxError || errorgmsmAgg > maxError)
    {
		printf("\nPrecision\n\tBetween CSR and hybrid:absolute %f, weighted %f\n", errorH, errorH/n);
		printf("\tBetween CSR and ELL:absolute %f, weighted %f\n", errorC, errorC/n);
		printf("\tBetween CSR and COO:absolute %f, weighted %f\n", errorO, errorO/n);
		printf("\tWith global memory\n\t\tAggregated: absolute %f, weighted %f\n", errorGagg, errorGagg/n);
		printf("\tWith global memory\n\t\tIrregular: absolute %f, weighted %f\n", errorirGagg, errorirGagg/n);
		printf("\tWith global memory\n\t\tWestphal: absolute %f, weighted %f\n", errorWestGagg, errorWestGagg/n);
		printf("\t\tNon aggregated: absolute %f, weighted %f\n", errorG, errorG/n);
		printf("\tWith  Agg+SM+GM memory\n\t\tAggregated: absolute %f, weighted %f\n", errorgmsmAgg , errorgmsmAgg/n);
		printf("\tWith  SM+GM memory\n\t\tAggregated: absolute %f, weighted %f\n", errorgmsm , errorgmsm/n);
    }

//    for ( int i = 0; i < n; i++ )
//   		printf("%d\t%f\t%f\t%f\n", i, hycoo[i], hyglobalatom[i], hyglobalaggatom[i]);

    results << endl;
	}

	printf("Bye\n");
	results.close();

    return 0;
}
