#ifndef AGGRGLOBAL_CUH_
#define AGGRGLOBAL_CUH_

#define WITHOUT_DOUBLE_ATOMIC 1
#define WITHOUT_SHUFFLE 0
#define WITHOUT_DOUBLE_SHUFFLE 0

#define WARP_SIZE 32
#define VIRT_WARP_SIZE 8

#if VIRT_WARP_SIZE == 1
#define MASK 0x00000001
#elif VIRT_WARP_SIZE == 2
#define MASK 0x00000003
#elif VIRT_WARP_SIZE == 4
#define MASK 0x0000000f
#elif VIRT_WARP_SIZE == 8
#define MASK 0x000000ff
#elif VIRT_WARP_SIZE == 16
#define MASK 0x0000ffff
#else
#define MASK 0xffffffff
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////   Aggregated and Global Memory: Only one leader ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ inline int lane_id(void) { return threadIdx.x % WARP_SIZE; }
__device__ inline int virt_lane_id(void) { return threadIdx.x % VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id(void) { return threadIdx.x / VIRT_WARP_SIZE; }
__device__ inline int virt_warp_id2(void) { return lane_id() / VIRT_WARP_SIZE; }

// Atomic add for doubles
#if WITHOUT_DOUBLE_ATOMIC
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                        __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

// Shuffle using doubles (Julien Demouth version)
#if WITHOUT_DOUBLE_SHUFFLE
__device__ __inline__ double __shfl(double x, int lane, int vw_size)
{
// Split the double number into 2 32b registers.
int lo, hi;
asm volatile( "mov.b64 {%0,%1}, %2;" : "=r"(lo), "=r"(hi) : "d"(x));
// Shuffle the two 32b registers.
 lo = __shfl(lo, lane, vw_size);
 hi = __shfl(hi, lane, vw_size);
// Recreate the 64b number.
asm volatile( "mov.b64 %0, {%1,%2};" : "=d"(x) : "r"(lo), "r"(hi));
return x;
}
#endif

// Warp broadcast
template <class T>
__device__ T warp_bcast(T v, int leader, int vw_size) {
#if WITHOUT_SHUFFLE
    __shared__ volatile T _v[BLOCK_SIZE/VIRT_WARP_SIZE]; // One element for each virtual warp

    if ( virt_lane_id() == leader )
    	_v[virt_warp_id()] = v;

    return _v[virt_warp_id()]; // No sync, unsafe
}
#else
	return __shfl(v, leader, vw_size);
}
#endif

// warp-aggregated atomic add (reduction, w/o return value)
template <class T>
__device__ inline void atomicAggAdd(T *ctr, T value){
#if WITHOUT_SHUFFLE
unsigned int vmask = __ballot(1) & ( MASK << (virt_warp_id2() * VIRT_WARP_SIZE)); // Consider active threads belonging to this virtual warp
// select the leader
int leader = __ffs(vmask) - 1; // leader will be 0 if all threads active
// leader does the update
T res = value;
int next = leader;

// Add value from every thread in the virtual warp
while(__popc(vmask)-1 > 0){
  // Update mask
  vmask ^= (1 << next);
  next = __ffs(vmask) - 1;

  int next2 = next % VIRT_WARP_SIZE;
  res += warp_bcast(value, next2, VIRT_WARP_SIZE);
}

if ( lane_id() == leader )
  atomicAdd(ctr, res);
}
#else
// leader is thread 0 of virtual warp
  T res = value;
  int next = 0;

  // Add value from every thread in the virtual warp
  while(next < VIRT_WARP_SIZE - 1){
    next++;
    res += warp_bcast(value, next, VIRT_WARP_SIZE);
  }

  if(virt_lane_id() == 0)
    atomicAdd(ctr, res);
}
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////// Irregular reduction in Global Memory: several leaders are sequentially processed ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename ValueType> 
__device__ void GM_aggregated_ireduction_float(ValueType *dst, const ValueType v, const int k) 
{
    int lane_id = threadIdx.x & 31;
      
	uint acc_mask;
	int leader = 0; 
	
	acc_mask = 0;

	while (leader >= 0){ // Process sequentially each leader

		const int k_leader = __shfl(k, leader, WARP_SIZE);
		
		const int dif = k - k_leader;
		uint mask = ~ __ballot(dif); // Threads with the same key valu3
	 
		ValueType v1;
		if (dif == 0)
			v1=v; // value is saved
		else
			v1=0;  // v1 is the thread value for the treads with the same key value, elsewhere 0
		
		const int lsb = __ffs(mask); // least significant bit position
		const int msb = WARP_SIZE - __clz(mask); // most significant bit position
		const int distance = msb-lsb; // distance between mosy distant bits
		
		ValueType vaux; // Shuffle output
		
		if (distance > 0) {
			vaux=__shfl_down(v1, 1, WARP_SIZE);
			if (lane_id < 31) // Their operation avoid adding data beyond warp limits
				v1 = vaux + v1;
		
			if (distance > 1) {
				vaux=__shfl_down(v1, 2, WARP_SIZE);
				if (lane_id < 30)
					v1 = vaux + v1;

				if (distance > 3) {
					vaux=__shfl_down(v1, 4, WARP_SIZE);
					if (lane_id < 28)
						v1 = vaux + v1;

					if (distance > 7) {
						vaux=__shfl_down(v1, 8, WARP_SIZE);
						if (lane_id < 24)
							v1 = vaux + v1;
		
						if (distance > 15) {
							vaux=__shfl_down(v1, 16, WARP_SIZE);
							if (lane_id < 16)
								v1 = vaux + v1;
						}
					}
				}
			}
		}
	
		if (lane_id == leader)
			atomicAdd(&dst[k], v1);
		
		acc_mask |= mask; // Lanes already processed
	
		leader = __ffs(~acc_mask)-1; 
	}
}


/****************************************************************************************/
/** Method proposed by Elmar Wetsphal: Voting and Shuffling for fewer Atomic Operations ***/
/******************************************************************************************/

template<typename IndexType> 
__device__ __inline__ uint get_peers(const IndexType my_key) { 
  uint peers; 
  bool is_peer; 
  uint unclaimed=0xffffffff;                   // in the beginning, no threads are claimed
  do {
    const IndexType other_key=__shfl(my_key,__ffs(unclaimed)-1);
// get key from least unclaimed lane 
    is_peer=(my_key==other_key);               // do we have a match?
    peers=__ballot(is_peer);
                   // find all matches                 
    unclaimed^=peers;                          // matches are no longer unclaimed
  } while (!is_peer);                          // repeat as long as we haven’t found our match
  return peers; 
}

template<typename IndexType, typename ValueType> 
__device__ __inline__ uint get_peers_and_vote(ValueType *dest, const IndexType my_key) { 
  uint peers; 
  bool is_peer; 
  int lane= threadIdx.x & 31; 
  uint unclaimed=0xffffffff;                   // in the beginning, no threads are claimed
  do {
	int leader = __ffs(unclaimed)-1;  // identify leader
    const IndexType other_key=__shfl(my_key, leader); 
// get key from least unclaimed lane 
    is_peer=(my_key==other_key);               // do we have a match?
    peers=__ballot(is_peer);
	int vote = __popc(peers);
	if (lane == leader)
		atomicAdd(dest+my_key, vote);
                   // find all matches                 
    unclaimed^=peers;                          // matches are no longer unclaimed
  } while (!is_peer);                          // repeat as long as we haven’t found our match
  return peers; 
}

template <typename IndexType,
			typename ValueType> 
__device__ __inline__ ValueType add_peers(ValueType *dest, const IndexType my_key, ValueType x, uint peers)
 { 
  int lane= threadIdx.x & 31; 
  int first=__ffs(peers)-1;              // find the leader
  int rel_pos=__popc(peers<<(32-lane));  // find our own place
  peers&=(0xfffffffe<<lane);             // drop everything to our right
  while(__any(peers)) {                  // stay alive as long as anyone is working
    int next=__ffs(peers);               // find out what to add
    ValueType t=__shfl(x,next-1);                // get what to add (undefined if nothing)
    if (next)                            // important: only add if there really is anything
      x+=t;
    int done=rel_pos&1;                  // local data was used in iteration when its LSB is set
    peers&=__ballot(!done);              // clear out all peers that were just used
    rel_pos>>=1;                         // count iterations by shifting position
  } 
  if (lane==first)                       // only leader threads for each key perform atomics 
    atomicAdd(dest+my_key,x);                   
	ValueType res=__shfl(x,first);				  // distribute result (if needed) 
  return res;                            // may also return x or return value of atomic, as needed
} 

template <typename IndexType,
			typename ValueType> 
__device__ __inline__ ValueType vote_peers_histo(ValueType *dest, const IndexType my_key, ValueType x, uint peers)
 { 
  int lane= threadIdx.x & 31; 
  int first=__ffs(peers)-1;              // find the leader
  int vote = __popc(peers);				 // count the number of threads voting in the same bin
  if (lane == first)		  			// only de leader
	atomicAdd(dest+my_key, vote);		// atomic update
  return 0;                            // may also return x or return value of atomic, as needed
}

#endif /* AGGRGLOBAL_CUH_ */
