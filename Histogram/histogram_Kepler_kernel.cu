////////////////////////////////////////////////////////////////////////////////
// Histogram - JGL - 21/08/2015
////////////////////////////////////////////////////////////////////////////////

#include "histogram_Kepler.h"
#include "AggrGlobal.cuh"

// Dynamic shared memory allocation
extern __shared__ unsigned int Hs[];

__global__ void histo_global(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
                atomicAdd(&histo[d], 1);
        }
}

__global__ void histo_R_per_block(unsigned int* histo, unsigned int* data, int size, int BINS, int BINSp, int R){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Offset to per-block sub-histograms
        const unsigned int off_rep = BINSp * (tx % R);
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Sub-histograms initialization
        for(int pos = tx; pos < BINSp*R; pos += blockDim.x) Hs[pos] = 0;
        __syncthreads();        // Intra-block synchronization
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
                atomicAdd(&Hs[off_rep + d], 1);
        }
        __syncthreads();      // Intra-block synchronization
        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                unsigned int sum = 0;
                for(int base = 0; base < BINSp*R; base += BINSp)
                        sum += Hs[base + pos];
                // Atomic addition in global memory
                atomicAdd(histo + pos, sum);
        }
}

__global__ void histo_Gert_Jan(unsigned int* histo, unsigned int* data, int size, int BINS, int R){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Offset to per-block sub-histograms
        const unsigned int off = tx % R;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Sub-histograms initialization
        for(int pos = tx; pos < BINS*R; pos += blockDim.x) Hs[pos] = 0;
        __syncthreads();        // Intra-block synchronization
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
                atomicAdd(&Hs[off + d * R], 1);
        }
        __syncthreads();        // Intra-block synchronization
        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                unsigned int sum = 0;
                for(int base = 0; base < R; base++)
                        sum += Hs[base + pos * R];
                // Atomic addition in global memory
                atomicAdd(histo + pos, sum);
        }
}

// Fixed XOR for bank and lock conflicts
__device__ unsigned int hash_xor(unsigned int address){
        unsigned int addr_xor = (address >> 5) & 0x3FF;
        addr_xor = addr_xor ^ address;
        return addr_xor;
}
__global__ void histo_R_per_block_Hash(unsigned int* histo, unsigned int* data, int size, int BINS, int BINSp, int R){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;

        // Offset to per-block sub-histograms
        const unsigned int off_rep = BINSp * (tx % R);

        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;

        // Sub-histograms initialization
        for(int pos = tx; pos < BINSp*R; pos += blockDim.x) Hs[pos] = 0;

        __syncthreads();        // Intra-block synchronization

        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
                atomicAdd(&Hs[hash_xor(off_rep + d)], 1);

        }

        __syncthreads();      // Intra-block synchronization

        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                unsigned int sum = 0;
                for(int base = 0; base < BINSp*R; base += BINSp)
                        sum += Hs[hash_xor(base + pos)];
                // Atomic addition in global memory
                atomicAdd(histo + pos, sum);
        }
}

__global__ void histo_Agg(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;

        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;

        // Sub-histograms initialization
        for(int pos = tx; pos < BINS; pos += blockDim.x) Hs[pos] = 0;

        __syncthreads();        // Intra-block synchronization

        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
                const unsigned int d_leader = warp_bcast(d, 0, VIRT_WARP_SIZE);
                if (d_leader == d)
                        atomicAggAdd<unsigned int>(&Hs[d], (unsigned int)1);
                else
                        atomicAdd(&Hs[d], (unsigned int)1);
        }
        __syncthreads();      // Intra-block synchronization
        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                // Atomic addition in global memory
                atomicAdd(histo + pos, Hs[pos]);
        }
}

__global__ void histo_Agg_G(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;

        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;

        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in global memory
                const unsigned int d_leader = warp_bcast(d, 0, VIRT_WARP_SIZE);
                if (d_leader == d)
                	atomicAggAdd<unsigned int>(&histo[d], (unsigned int)1);
                else
                        atomicAdd(&histo[d], (unsigned int)1);
        }
}

__global__ void histo_Guil(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Sub-histograms initialization
        for(int pos = tx; pos < BINS; pos += blockDim.x) Hs[pos] = 0;
        __syncthreads();        // Intra-block synchronization
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
		GM_aggregated_ireduction_float<unsigned int>(Hs, (unsigned int)1, d);
        }
        __syncthreads();      // Intra-block synchronization
        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                // Atomic addition in global memory
                atomicAdd(histo + pos, Hs[pos]);
        }
}

__global__ void histo_Guil_G(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in global memory
		GM_aggregated_ireduction_float<unsigned int>(histo, (unsigned int)1, d);
        }
}

__global__ void histo_West(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Sub-histograms initialization
        for(int pos = tx; pos < BINS; pos += blockDim.x) Hs[pos] = 0;
        __syncthreads();        // Intra-block synchronization
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in shared memory
    
				get_peers_and_vote<unsigned int, unsigned int>(Hs, d);
    
        }
        __syncthreads();      // Intra-block synchronization
        // Merge per-block histograms and write to global memory
        for(int pos = tx; pos < BINS; pos += blockDim.x){
                // Atomic addition in global memory
                atomicAdd(histo + pos, Hs[pos]);
        }
}

__global__ void histo_West_G(unsigned int* histo, unsigned int* data, int size, int BINS){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
        // Main loop
        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN
                unsigned int d = ((data[i] * BINS) >> 12);
#else
                unsigned int d = data[i];
#endif
                // Atomic vote in global memory
				
				get_peers_and_vote<unsigned int>(histo, d);
               
        }
}

__global__ void histo_R_per_block_Kepler(unsigned int* histo, unsigned int* data, int size, int BINS, int BINSp, int R){
        // Block and thread index
        const int bx = blockIdx.x;
        const int tx = threadIdx.x;
        // Constants for naive read access
        const int begin = bx * blockDim.x + tx;
        const int end = size;
        const int step = blockDim.x * gridDim.x;
#if !GLOBAL
        // Sub-histograms initialization
        //for(int pos = tx; pos < BINS; pos += blockDim.x) Hs[pos] = 0; // (a) 1-per-block
        const unsigned int off_rep = BINSp * (virt_warp_id() % R); // (b)
        //const unsigned int off_rep = BINSp * (lane_id() % R); // (c)
        //const unsigned int off_rep = virt_warp_id() % R; // (d)
        for(int pos = tx; pos < BINSp*R; pos += blockDim.x) Hs[pos] = 0; // (b, c, d) R-per-block
        __syncthreads();        // Intra-block synchronization
#endif
        const unsigned int mask_white = MASK << (virt_warp_id2() * VIRT_WARP_SIZE);

        for(int i = begin; i < end; i += step){
                // Global memory read
#if VANHATEREN          
                int value_A = (int)((data[i] * BINS) >> 12);
#else
                int value_A = (int)data[i];
#endif
                int value_0 = warp_bcast(value_A, 0, VIRT_WARP_SIZE); // Leader value 
                unsigned int ball_0 = (unsigned int)__ballot(value_0 == value_A); // Bit N=1 if pred=true for thread N 
                unsigned int ball_1 = ball_0 & mask_white;   // Mask according to virtual warp size
                unsigned int sum = (unsigned int)__popc(ball_1); // Population count
#if !GLOBAL
                if(virt_lane_id() == 0)
                        //atomicAdd(Hs + value_A, sum); // (a)
                        atomicAdd(Hs + off_rep + value_A, sum); // (b, c)
                        //atomicAdd(Hs + off_rep + R*value_A, sum); // (d)
                else if(value_0 != value_A)
                        //atomicAdd(Hs + value_A, 1); // (a)
                        atomicAdd(Hs + off_rep + value_A, 1); // (b, c)
                        //atomicAdd(Hs + off_rep + R*value_A, 1); // (d)
#else
                if(virt_lane_id() == 0)
                        atomicAdd(histo + value_A, sum);
                else if(value_0 != value_A)
                        atomicAdd(histo + value_A, 1);
#endif
        }
#if !GLOBAL
        __syncthreads();
        // Merge per-block histograms and write to global memory
        /*for(int pos = tx; pos < BINS; pos += blockDim.x){     // (a) 1-per-block
                // Atomic addition in global memory
                atomicAdd(histo + pos, Hs[pos]);
        }*/
        for(int pos = tx; pos < BINS; pos += blockDim.x){ // (b, c, d)
                unsigned int sum = 0;
                for(int base = 0; base < BINSp*R; base += BINSp)
                        sum += Hs[base + pos];
                // Atomic addition in global memory
                atomicAdd(histo + pos, sum);
        }
#endif
}

