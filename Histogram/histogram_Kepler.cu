////////////////////////////////////////////////////////////////////////////////
// Histogram - JGL - Nov./02/2014
// Monochrome image
// R-per-block: Cyclic, block, group (GPU-vote), and aggregated atomics
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
// Common host and device functions
////////////////////////////////////////////////////////////////////////////////
//Round a / b to nearest higher integer value
int iDivUp(int a, int b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}
//Round a / b to nearest lower integer value
int iDivDown(int a, int b){
    return a / b;
}
//Align a to nearest higher multiple of b
int iAlignUp(int a, int b){
    return (a % b != 0) ?  (a - a % b + b) : a;
}
//Align a to nearest lower multiple of b
int iAlignDown(int a, int b){
    return a - a % b;
}

#define ByteSwap16(n) ( ((((unsigned int) n) << 8) & 0xFF00) | ((((unsigned int) n) >> 8) & 0x00FF) )

////////////////////////////////////////////////////////////////////////////////
// Reference CPU histogram
////////////////////////////////////////////////////////////////////////////////
extern "C" void histo_CPU(unsigned int *histo, unsigned int *data, int size, int BINS);

///////////////////////////////////////////////////////////////////////////////
// GPU kernels
////////////////////////////////////////////////////////////////////////////////
#include "histogram_Kepler_kernel.cu"

//Carry out dummy calculations before main computation loop
//in order to "warm up" the hardware/driver
#define WARMUP

////////////////////////////////////////////////////////////////////////////////
// Main program
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv){
	const int DATA_W = 1536;//512;
	const int DATA_H = 1024;//512;
	unsigned int *h_DataA;
	unsigned int *hh_DataA; 
	unsigned int *d_DataA; 
	unsigned int *d_Temp;

	unsigned int *d_histo_block5, *d_histo_block4, *d_histo_block3, *d_histo_block2, *d_histo_block;
	unsigned int *d_histo_block_Guil, *d_histo_block_Guil_G, *d_histo_block_West, *d_histo_block_West_G, *d_histo_block_Kepler, *d_histo_block_global;

	unsigned int *h_ResultCPU, *h_ResultGPU_block5, *h_ResultGPU_block4, *h_ResultGPU_block3, *h_ResultGPU_block2, *h_ResultGPU_block;
	unsigned int *h_ResultGPU_block_Guil, *h_ResultGPU_block_Guil_G, *h_ResultGPU_block_West, *h_ResultGPU_block_West_G, *h_ResultGPU_block_Kepler, *h_ResultGPU_block_global;

	float time_histo_block, time_block5, time_block4, time_block3, time_block2, time_block;
	time_histo_block = 0; time_block5 = 0; time_block2 = 0; time_block3 = 0; time_block4 = 0; time_block = 0; 
	float time_block_Guil, time_block_Guil_G, time_block_West, time_block_West_G, time_block_Kepler, time_block_global;
	time_block_Guil = 0; time_block_Guil_G = 0; time_block_West = 0; time_block_West_G = 0; time_block_Kepler = 0; time_block_global = 0;

	//unsigned int hTimer;
	//double time_cpu = 0;
	double sum_delta2,sum_ref2,L1norm2;

	// Syntax verification
	if (argc != 11) {
		printf("Wrong format\n");
		printf("Syntax: %s <Device Path BINS DATA_SIZE NUM_BLOCKS BLOCKS_TH BLOCKS_SO BLOCK_HIST REP pad S repeat>\n",argv[0]);
		exit(1);
	}

	// Check the compute capability of the device
	int num_devices=0;
	cudaGetDeviceCount(&num_devices);
	if(0==num_devices){
		printf("Your system does not have a CUDA capable device\n");
		return 1;
	}
	int device = atoi(argv[1]);
	cudaDeviceProp device_properties;
	cudaGetDeviceProperties(&device_properties,device);
	if((1==device_properties.major)&&(device_properties.minor<1))
		printf("%s does not have compute capability 1.1 or later\n\n",device_properties.name);

	// Set device
	cudaSetDevice(device);

	int BINS = atoi(argv[3]);
	int DATA_SIZE = DATA_W * DATA_H;
	//int DATA_SIZE = iAlignUp(atoi(argv[4]), BINS);
	//int DATA_SIZE = atoi(argv[4]);
	int NUM_BLOCKS = atoi(argv[5]);
	int BLOCK_HIST = atoi(argv[6]);
	int REP = atoi(argv[7]);	// Replication factor
	int REPgj = REP;
	int pad = atoi(argv[8]);	// Pad size
	char S = *argv[9];
	int repeat = atoi(argv[10]);

	int BINSp = BINS + pad;

	const int DATA_SIZE_INT = DATA_SIZE * sizeof(unsigned int);

	printf("%s\t%s\t%s\tNum_frames=%d\tBins=%d\tImage_size=%d\tNum_blocks=%d\tNum_threads=%d\tReplication_factor=%d\tPadding_size=%d\tS=%c\tVirt_warp=%d\t", argv[0], argv[2], device_properties.name, repeat, BINS, DATA_SIZE, NUM_BLOCKS, BLOCK_HIST, REP, pad, S, VIRT_WARP_SIZE);

	// Host memory allocation
	cudaMallocHost((void **)&h_DataA, DATA_SIZE_INT);
	cudaMallocHost((void **)&hh_DataA, DATA_SIZE_INT);

    cudaMallocHost((void **)&h_ResultCPU, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block5, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block4, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block3, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block2, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_Guil, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_Guil_G, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_West, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_West_G, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_Kepler, BINS*sizeof(unsigned int));
    cudaMallocHost((void **)&h_ResultGPU_block_global, BINS*sizeof(unsigned int));

	// Device memory allocation
	 cudaMalloc((void **)&d_DataA, DATA_SIZE_INT);

     cudaMalloc((void **)&d_Temp, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block5, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block4, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block3, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block2, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_Guil, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_Guil_G, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_West, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_West_G, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_Kepler, BINS*sizeof(unsigned int));
     cudaMalloc((void **)&d_histo_block_global, BINS*sizeof(unsigned int));

	// Initizalize host and device arrays
	memset(h_ResultCPU,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block5,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block4,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block3,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block2,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_Guil,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_Guil_G,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_West,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_West_G,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_Kepler,0,BINS*sizeof(unsigned int));
	memset(h_ResultGPU_block_global,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block5,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block4,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block3,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block2,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_Guil,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_Guil_G,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_West,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_West_G,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_Kepler,0,BINS*sizeof(unsigned int));
	 cudaMemset(d_histo_block_global,0,BINS*sizeof(unsigned int));

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Grids and blocks
	///////////////////////////////////////////////////////////////////////////////////////////////////
	dim3 blockGrid_histo(NUM_BLOCKS);
	dim3 threadBlock_histo(BLOCK_HIST);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WARMUP
        // RANDOM WORKLOAD
        srand(time(NULL));
        for(int i=0; i<DATA_SIZE; ++i){ // Pixels are stored in 4 bytes
                hh_DataA[i] = (unsigned int)(BINS*(((float)rand()/(float)RAND_MAX)));
        }
        // Host to device memory
        cudaMemcpy(d_DataA, hh_DataA, DATA_SIZE_INT, cudaMemcpyHostToDevice);
        //printf("Warm up...\n");
        //histogram_R_per_block(d_Temp, d_DataA, DATA_SIZE, BINS, BINSp, REP, NUM_BLOCKS, BLOCK_HIST);
        //histo_R_per_block<<<blockGrid_histo,threadBlock_histo,(BINSp*REP)*sizeof(int)>>>(
        //        d_Temp,d_DataA,DATA_SIZE,BINS,BINSp,REP);
        //cudaThreadSynchronize();
#endif
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

    char dctFileName[100];
    FILE *File;
	
for(int i = 0; i < repeat; i++){
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Input image, from file
	///////////////////////////////////////////////////////////////////////////////////////////////////
        // Van Hateren's images
#if VANHATEREN
        unsigned short temp;
        sprintf(dctFileName,"%s%d.iml",argv[2],i);
        if((File = fopen(dctFileName, "rb")) != NULL){
                for (int y=0; y < DATA_SIZE; y++){
                        fread(&temp, sizeof(unsigned short), 1, File);
                        hh_DataA[y] = (unsigned int)ByteSwap16(temp);
                        if(hh_DataA[y] >= 4096) hh_DataA[y] = 4095;
                }
                fclose(File);
        }
        else{
                printf("%s does not exist\n", dctFileName);
                exit(1);
        }
#else
if(S == 'l'){
   	sprintf(dctFileName,"%s",argv[2]);
	if ((File = fopen(dctFileName,"r")) != NULL){
		for (int y=0; y<DATA_SIZE; ++y){
			float temp;
			fscanf(File,"%f ",&temp);
			//printf("%u\n",*(h_Lect+y));
			hh_DataA[y] = (unsigned int)temp;
       	}
    	fclose(File);
    	}
	else{
		printf("%s does not exist\n", dctFileName);
		exit(1);
	}
}
        // Random input
else if(S == 'r'){
        srand(i);
        for(int y=0; y<DATA_SIZE; ++y){ // Pixels are stored in 4 bytes
                hh_DataA[y] = (unsigned int)(BINS*(((float)rand()/(float)RAND_MAX)));
        }
}
        // Synthetic input
else if(S == 'b'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = y % BINS;
        }
}
else if(S == 'w'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = 0;
        }
}
else if(S == 'c'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = (y/2)%BINS;
        }
}
else if(S == 'd'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = (y/4)%BINS;
        }
}
else if(S == 'e'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = (y/8)%BINS;
        }
}
else if(S == 'f'){
        for(int y=0; y<DATA_SIZE; ++y){
                hh_DataA[y] = (y/16)%BINS;
        }
}
#endif
        /*printf("\n");
        for(int j=0; j<256; j++){
                if(j%32==0) printf("\n");
                printf("%u\t", hh_DataA[j]);
        }
        printf("\n");*/


	// Host to device memory
	 cudaMemcpy(d_DataA, hh_DataA, DATA_SIZE_INT, cudaMemcpyHostToDevice);

        time_histo_block = 0;
	// Warm-up
#if !GLOBAL
        histo_R_per_block<<<blockGrid_histo,threadBlock_histo,(BINSp*REP)*sizeof(int)>>>(
                d_Temp, d_DataA, DATA_SIZE, BINS, BINSp, REP);
	cudaDeviceSynchronize();
#endif

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTOGRAM IN GLOBAL MEMORY
	///////////////////////////////////////////////////////////////////////////////////////////////////
        cudaEventRecord( start, 0 );
        histo_global<<<blockGrid_histo,threadBlock_histo>>>(
                d_histo_block_global, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_global += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_global,d_histo_block_global,BINS*sizeof(int),cudaMemcpyDeviceToHost);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTOGRAM IN SHARED MEMORY
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// R-per-block - Cyclic
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_R_per_block<<<blockGrid_histo,threadBlock_histo,(BINSp*REP)*sizeof(int)>>>(
                d_histo_block, d_DataA, DATA_SIZE, BINS, BINSp, REP);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block += time_histo_block;

        cudaMemcpy(h_ResultGPU_block,d_histo_block,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

	// GPU-Vote
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_Gert_Jan<<<blockGrid_histo,threadBlock_histo,(BINS*REPgj)*sizeof(int)>>>(
                d_histo_block4, d_DataA, DATA_SIZE, BINS, REPgj);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block3 += time_histo_block;

        cudaMemcpy(h_ResultGPU_block4,d_histo_block4,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // XOR APPROACH
        ///////////////////////////////////////////////////////////////////////////////////////////////////
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_R_per_block_Hash<<<blockGrid_histo,threadBlock_histo,BINS*REP*sizeof(int)>>>(
                d_histo_block5, d_DataA, DATA_SIZE, BINS, BINS, REP);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block5 += time_histo_block;

        cudaMemcpy(h_ResultGPU_block5,d_histo_block5,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

        ///////////////////////////////////////////////////////////////////////////////////////////////////
	// Aggregated atomics
        ///////////////////////////////////////////////////////////////////////////////////////////////////
	// Global Agg
	// Shared mem.
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_Agg<<<blockGrid_histo,threadBlock_histo,BINS*sizeof(int)>>>(
                d_histo_block2, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block4 += time_histo_block;

        cudaMemcpy(h_ResultGPU_block2,d_histo_block2,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

	// Global mem.
        cudaEventRecord( start, 0 );
        histo_Agg_G<<<blockGrid_histo,threadBlock_histo>>>(
                d_histo_block3, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block2 += time_histo_block;

        cudaMemcpy(h_ResultGPU_block3,d_histo_block3,BINS*sizeof(int),cudaMemcpyDeviceToHost);

	// Guil
	// Shared mem.
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_Guil<<<blockGrid_histo,threadBlock_histo,BINS*sizeof(int)>>>(
                d_histo_block_Guil, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_Guil += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_Guil,d_histo_block_Guil,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

	// Global mem.
        cudaEventRecord( start, 0 );
        histo_Guil_G<<<blockGrid_histo,threadBlock_histo>>>(
                d_histo_block_Guil_G, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_Guil_G += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_Guil_G,d_histo_block_Guil_G,BINS*sizeof(int),cudaMemcpyDeviceToHost);

	// Westphal
	// Shared mem.
#if !GLOBAL
        cudaEventRecord( start, 0 );
        histo_West<<<blockGrid_histo,threadBlock_histo,BINS*sizeof(int)>>>(
                d_histo_block_West, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_West += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_West,d_histo_block_West,BINS*sizeof(int),cudaMemcpyDeviceToHost);
#endif

        cudaEventRecord( start, 0 );
        histo_West_G<<<blockGrid_histo,threadBlock_histo>>>(
                d_histo_block_West_G, d_DataA, DATA_SIZE, BINS);
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_West_G += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_West_G,d_histo_block_West_G,BINS*sizeof(int),cudaMemcpyDeviceToHost);

	// Kepler
        cudaEventRecord( start, 0 );
#if GLOBAL
        histo_R_per_block_Kepler<<<blockGrid_histo,threadBlock_histo>>>(
                d_histo_block_Kepler, d_DataA, DATA_SIZE, BINS, BINSp, REP);
#else
        histo_R_per_block_Kepler<<<blockGrid_histo,threadBlock_histo,REP*BINSp*sizeof(int)>>>(     // 1-per-block
                d_histo_block_Kepler, d_DataA, DATA_SIZE, BINS, BINSp, REP);
#endif
        cudaEventRecord( stop, 0 );
        cudaEventSynchronize( stop );
        cudaEventElapsedTime( &time_histo_block, start, stop );
        time_block_Kepler += time_histo_block;

        cudaMemcpy(h_ResultGPU_block_Kepler,d_histo_block_Kepler,BINS*sizeof(int),cudaMemcpyDeviceToHost);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CPU HISTOGRAM
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Start CPU timer
	cudaThreadSynchronize();	// Thread synchronization
	//CUT_SAFE_CALL(cutResetTimer(hTimer));
	//CUT_SAFE_CALL(cutStartTimer(hTimer));

	histo_CPU(h_ResultCPU, hh_DataA, DATA_SIZE, BINS);

	// Stop CPU timer
	// cudaThreadSynchronize());	// Thread synchronization
	//CUT_SAFE_CALL(cutStopTimer(hTimer));
	//time_cpu += cutGetTimerValue(hTimer);

	/*printf("CPU\n");
	for(int y=0; y<BINS; ++y){
		printf("%d ",h_ResultCPU[y]);
	}*/
#define PRINT 0
#if PRINT
	printf("R-per-block\n");
	for(int y=0; y<BINS; ++y){
                if(y%32==0) printf("\n");
		printf("%d\t",h_ResultGPU_block[y]);
	}
	printf("\n");
	printf("Agg\n");
	for(int y=0; y<BINS; ++y){
                if(y%32==0) printf("\n");
		printf("%d\t",h_ResultGPU_block2[y]);
	}
	printf("\n");
	printf("Agg_G\n");
	for(int y=0; y<BINS; ++y){
                if(y%32==0) printf("\n");
		printf("%d\t",h_ResultGPU_block3[y]);
	}
	printf("\n");
#endif
}

printf("\t");
printf("R_per_block_Cyclic =\t%f\n",time_block/repeat);
printf("Gert-Jan =\t%f\n",time_block3/repeat);
printf("R_per_block_Hash =\t%f\n",time_block5/repeat);
printf("Agg =\t%f\n",time_block4/repeat);
printf("Agg_G =\t%f\n",time_block2/repeat);
printf("Guil =\t%f\n",time_block_Guil/repeat);
printf("Guil_G =\t%f\n",time_block_Guil_G/repeat);
printf("West =\t%f\n",time_block_West/repeat);
printf("West_G =\t%f\n",time_block_West_G/repeat);
printf("Kepler =\t%f\n",time_block_Kepler/repeat);
printf("Global =\t%f\n",time_block_global/repeat);
printf("\n");
//printf("histo_cpu =\t%f\t",time_cpu/repeat);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Comparing the results
	///////////////////////////////////////////////////////////////////////////////////////////////////
	sum_delta2 = 0;
	sum_ref2   = 0;
	L1norm2 = 0;
	for(int i = 0; i < BINS; i++){
		sum_delta2 += fabs(h_ResultGPU_block[i] - h_ResultCPU[i]);
    	sum_ref2   += fabs(h_ResultCPU[i]);
	}
	L1norm2 = (double)(sum_delta2 / sum_ref2);
	printf((L1norm2 < 1) ? "BLOCK -- TEST PASSED\t" : "TEST FAILED\t");

	sum_delta2 = 0;
    sum_ref2   = 0;
	L1norm2 = 0;
    for(int i = 0; i < BINS; i++){
    	sum_delta2 += fabs(h_ResultGPU_block4[i] - h_ResultCPU[i]);
       	sum_ref2   += fabs(h_ResultCPU[i]);
    }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Gert-Jan -- TEST PASSED\t" : "TEST FAILED\t");

	sum_delta2 = 0;
    sum_ref2   = 0;
	L1norm2 = 0;
    for(int i = 0; i < BINS; i++){
    	sum_delta2 += fabs(h_ResultGPU_block5[i] - h_ResultGPU_block[i]);
       	sum_ref2   += fabs(h_ResultGPU_block[i]);
    }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1) ? "HASH -- TEST PASSED\t" : "TEST FAILED\t");

	sum_delta2 = 0;
    sum_ref2   = 0;
	L1norm2 = 0;
	for(int i = 0; i < BINS; i++){
		sum_delta2 += fabs(h_ResultGPU_block2[i] - h_ResultGPU_block[i]);
	    sum_ref2   += fabs(h_ResultGPU_block[i]);
	}
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Agg -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block3[i] - h_ResultCPU[i]);
            sum_ref2   += fabs(h_ResultCPU[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Agg_G -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_Guil[i] - h_ResultGPU_block[i]);
            sum_ref2   += fabs(h_ResultGPU_block[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Guil -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_Guil_G[i] - h_ResultCPU[i]);
            sum_ref2   += fabs(h_ResultCPU[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Guil_G -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_West[i] - h_ResultGPU_block[i]);
            sum_ref2   += fabs(h_ResultGPU_block[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "West -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_West_G[i] - h_ResultCPU[i]);
            sum_ref2   += fabs(h_ResultCPU[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "West_G -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_Kepler[i] - h_ResultCPU[i]);
            sum_ref2   += fabs(h_ResultCPU[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Kepler -- TEST PASSED\t" : "TEST FAILED\t");

        sum_delta2 = 0;
    sum_ref2   = 0;
        L1norm2 = 0;
        for(int i = 0; i < BINS; i++){
                sum_delta2 += fabs(h_ResultGPU_block_global[i] - h_ResultCPU[i]);
            sum_ref2   += fabs(h_ResultCPU[i]);
        }
    L1norm2 = (double)(sum_delta2 / sum_ref2);
    printf((L1norm2 < 1e-6) ? "Global -- TEST PASSED\t" : "TEST FAILED\t");

	printf("\n");
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Free memory
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Device memory
	 cudaFree(d_Temp);
     cudaFree(d_DataA);
	 cudaFree(d_histo_block5);
	 cudaFree(d_histo_block4);
	 cudaFree(d_histo_block3);
	 cudaFree(d_histo_block2);
	 cudaFree(d_histo_block);
	 cudaFree(d_histo_block_Guil);
	 cudaFree(d_histo_block_Guil_G);
	 cudaFree(d_histo_block_West);
	 cudaFree(d_histo_block_West_G);
	 cudaFree(d_histo_block_Kepler);
	 cudaFree(d_histo_block_global);
	// Host memory
	cudaFreeHost(h_DataA);
	cudaFreeHost(hh_DataA);
	cudaFreeHost(h_ResultGPU_block5);
	cudaFreeHost(h_ResultGPU_block4);
	cudaFreeHost(h_ResultGPU_block3);
	cudaFreeHost(h_ResultGPU_block2);
	cudaFreeHost(h_ResultGPU_block);
	cudaFreeHost(h_ResultGPU_block_Guil);
	cudaFreeHost(h_ResultGPU_block_Guil_G);
	cudaFreeHost(h_ResultGPU_block_West);
	cudaFreeHost(h_ResultGPU_block_West_G);
	cudaFreeHost(h_ResultGPU_block_Kepler);
	cudaFreeHost(h_ResultGPU_block_global);
	cudaFreeHost(h_ResultCPU);
	cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
